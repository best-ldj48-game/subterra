﻿namespace CityFall
{
    public static class SceneHelper
    {
        public const string GameScene = "Game";

        public const string MainMenuScene = "MainMenu";

        public const string StoryScene = "Story";

        public const string TutorialScene = "Tutorial";
    }
}