namespace CityFall
{
    /// <summary>
    ///     Singleton pattern
    /// </summary>
    public static class Singleton<T> where T : class
    {
        private static T _singleton;

        public static T Get()
        {
            return _singleton;
        }

        public static bool TryGet(out T singleton)
        {
            singleton = _singleton;
            return singleton != null;
        }

        public static bool Exists()
        {
            return _singleton != null;
        }

        public static void Set(T singleton)
        {
            _singleton = singleton;
        }
    }
}