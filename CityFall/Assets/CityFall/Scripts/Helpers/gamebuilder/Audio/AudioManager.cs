﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBuilder.Audio {
    public class AudioManager : MonoBehaviour {

        public class AudioSourceManager {
            public AudioManager jigsawAudioManager;
            public AudioSource audioSource;
            public float normVolume;

            public void Update() {
                if (!audioSource.isPlaying) {
                    jigsawAudioManager.ReleaseAudioManager(this);
                }
            }
        }
        public List<AudioSourceManager> availableAudioSources;
        public List<AudioSourceManager> usedAudioSources;

        public AudioSourceManager CreateNewAudioSource() {
            AudioSource audioSource = new GameObject().AddComponent<AudioSource>();
            audioSource.loop = false;
            audioSource.transform.SetParent(transform, false);

            AudioSourceManager audioSourceManager = new AudioSourceManager();
            audioSourceManager.jigsawAudioManager = this;
            audioSourceManager.audioSource = audioSource;

            return audioSourceManager;
        }
        public AudioSourceManager GetAudioSource() {
            AudioSourceManager audioManager = null;

            if (availableAudioSources.Count != 0) {
                audioManager = availableAudioSources[0];
                availableAudioSources.RemoveAt(0);
            } else {
                audioManager = CreateNewAudioSource();
            }
            return audioManager;
        }
        public void ReleaseAudioManager(AudioSourceManager audioSourceManager) {
            audioSourceManager.audioSource.Stop();
            int indexOfAudioSourceManager = usedAudioSources.IndexOf(audioSourceManager);
            if (indexOfAudioSourceManager >= 0) {
                usedAudioSources.RemoveAt(indexOfAudioSourceManager);
                availableAudioSources.Add(audioSourceManager);
            }
        }

        public virtual void Awake() {
            availableAudioSources = new List<AudioSourceManager>();
            usedAudioSources = new List<AudioSourceManager>();
        }


        public virtual void Update() {
            for (int i = usedAudioSources.Count - 1; i >= 0; i--) {
                usedAudioSources[i].Update();
            }
        }
    }
}