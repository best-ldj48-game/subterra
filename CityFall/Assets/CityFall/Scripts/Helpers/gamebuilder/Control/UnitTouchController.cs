﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using GameBuilder;

//namespace GameBuilder.Control {
//    /*
//     * UnitTouchController is a generic controler for both mouse and touch controls
//     */
//    public class ClickableButton : MonoBehaviour {
//        public MouseSource mouseSource;

//        public virtual void SetMouseSource2D(MouseSource mouseSource) {
//            this.mouseSource = mouseSource;
//        }

//        public virtual void Clicked() {

//        }
//        public virtual void Clicked(MouseSource mouseSource2D) {

//        }


//        public virtual void Released() {
//            UnSelected();
//        }
//        public virtual void Released(MouseSource mouseSource2D) {

//        }


//        public virtual Collider GetCollider() {
//            return null;
//        }
//        public virtual void UnSelected() {

//        }
//        public virtual void UnSelected(MouseSource mouseSource2D) {

//        }
//        /// <summary>
//        /// Short click, button
//        /// </summary>
//        public virtual void Pressed() {

//        }
//        /// <summary>
//        /// Quickly unselected
//        /// </summary>
//        public virtual void QuickUnSelected() {

//        }
//    }
//    public abstract class MouseSource {
//        public const float timeForPress = 0.15f;

//        public ClickableButton buttonSelected;
//        public UnitTouchController belongingTouchControls;

//        public float selectedObjectZ;
//        public Vector2 downWorldPos;
//        public float clickStartTime;
//        public Vector2 lastFrameWorldPos;

//        public abstract Vector2 WorldPos();
//        public abstract Vector2 StartWorldPos();
//        public abstract Vector2 NormPos();
//        public abstract Vector2 PixelPos();

//        public abstract Vector2 Delta();
//        public abstract void ReCast();

//        public float TimeSinceClickStart() {
//            return Time.unscaledTime - clickStartTime;
//        }
//        public virtual void Release() {
//            Vector2 worldPos = WorldPos();

//            if (buttonSelected != null) {
//                Collider2D buttonColl = buttonSelected.GetCollider();
//                bool quickClick = TimeSinceClickStart() < timeForPress;

//                if (buttonColl != null) {
//                    bool colEnabled = buttonColl.enabled;
//                    buttonColl.enabled = true;

//                    if (buttonColl.OverlapPoint(worldPos)) {
//                        buttonColl.enabled = colEnabled;
//                        if (quickClick) {
//                            buttonSelected.Pressed();
//                        }
//                        buttonSelected.Released();
//                        buttonSelected.Released(this);
//                    } else {
//                        buttonColl.enabled = colEnabled;
//                    }
//                }
//                if (quickClick)
//                    buttonSelected.QuickUnSelected();
//                buttonSelected.UnSelected();
//                buttonSelected.UnSelected(this);

//                if (buttonSelected.mouseSource == this)
//                    buttonSelected.SetMouseSource2D(null);
//            }
//            buttonSelected = null;
//        }
//        public virtual void LateUpdate() {
//            lastFrameWorldPos = WorldPos();
//        }
//        public virtual Vector2 LastFrameDelta() {
//            return WorldPos() - lastFrameWorldPos;
//        }

//        public virtual void SelectButton(ClickableButton button) {
//            Release();
//            lastFrameWorldPos = WorldPos();
//            buttonSelected = button;

//            if (button == null)
//                return;

//            clickStartTime = Time.unscaledTime;

//            Collider coll = button.GetCollider();

//            if (coll)
//                selectedObjectZ = coll.transform.position.z;

//            buttonSelected.SetMouseSource(this);
//            buttonSelected.Clicked();
//            buttonSelected.Clicked(this);
//        }
//    }

//    public class UnitTouchController : MonoBehaviour {
//        public class FingerTouch : MouseSource2D {
//            public Touch touch;
//            public Touch currentTouch;


//            public override Vector2 WorldPos() {
//                return UnitTouchController2D.TouchToWorldPos(currentTouch);
//            }
//            public override Vector2 NormPos() {
//                return currentTouch.NormalizedPosition();
//            }
//            public override Vector2 PixelPos() {
//                return currentTouch.position;
//            }
//            public override Vector2 StartWorldPos() {
//                return downWorldPos;
//            }

//            public override Vector2 Delta() {
//                return WorldPos() - StartWorldPos();
//            }
//            public Vector2 MouseSpeed() {
//                return currentTouch.CurrentVel();
//            }
//            public override void ReCast() {
//                Collider2D coll = UnitTouchController2D.OverlapPoint(WorldPos());

//                if (coll) {
//                    ClickableButton2D guiButton = coll.GetComponent<ClickableButton2D>();
//                    this.SelectButton(guiButton);
//                }
//            }
//        }
//        public class MouseClick : MouseSource2D {
//            public override Vector2 WorldPos() {
//                return ScreenToWorldPos(Input.mousePosition);
//            }
//            public override Vector2 NormPos() {
//                return HelperFunctions.MouseNormalizedPosition();
//            }
//            public override Vector2 PixelPos() {
//                return Input.mousePosition;
//            }
//            public override Vector2 StartWorldPos() {
//                return downWorldPos;
//            }
//            public override Vector2 Delta() {
//                return WorldPos() - StartWorldPos();
//            }
//            public override void ReCast() {
//                Collider2D coll = UnitTouchController2D.OverlapPoint(WorldPos());

//                if (coll) {
//                    ClickableButton2D guiButton = coll.GetComponent<ClickableButton2D>();
//                    this.SelectButton(guiButton);
//                }
//            }
//        }

//        public List<FingerTouch> activeTouches;
//        public MouseClick mouseClick;
//        public static Camera cam;

//        public static Vector2 ScreenToWorldPos(Vector2 screenPos) {
//            return cam.ScreenToWorldPoint(screenPos);
//        }
//        public static Vector2 TouchToWorldPos(Touch worldPos) {
//            return ScreenToWorldPos(worldPos.position);
//        }
//        public static Ray ScreenToRay(Vector2 position) {
//            Ray hit = Camera.main.ScreenPointToRay(position);
//            return hit;
//        }
//        private void Start() {
//            activeTouches = new List<FingerTouch>();
//            cam = GetComponentInChildren<Camera>(true);
//            Input.simulateMouseWithTouches = false;
//            mouseClick = new MouseClick();
//            mouseClick.belongingTouchControls = this;
//        }
//        void Update() {
//            for (int i = 0; i < Input.touchCount; i++) {
//                Touch touch = Input.GetTouch(i);
//                int touchActiveIndex = IndexOfTouch(touch.fingerId);
//                if (touchActiveIndex >= 0) {
//                    activeTouches[touchActiveIndex].currentTouch = touch;
//                } else if (!UIGraphicRaycastCatcher.PointerNotTouchingUI(touch.fingerId)) {
//                    continue;
//                }
//                if (touch.phase == TouchPhase.Began) {
//                    RemoveFinger(touch);
//                    AddFingerClick(touch);
//                } else if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended) {
//                    RemoveFinger(touch);
//                }
//            }

//            if (Input.GetKeyDown(KeyCode.Mouse0) && UIGraphicRaycastCatcher.MouseNotTouchingUI()) {
//                mouseClick.downWorldPos = ScreenToWorldPos(Input.mousePosition);
//                Collider2D coll = OverlapPoint(mouseClick.downWorldPos);

//                if (coll) {
//                    ClickableButton2D guiButton = coll.GetComponent<ClickableButton2D>();
//                    mouseClick.SelectButton(guiButton);
//                }
//            }
//            if (Input.GetKeyUp(KeyCode.Mouse0)) {
//                mouseClick.Release();
//            }
//        }
//        void LateUpdate() {
//            mouseClick.LateUpdate();
//            for (int i = 0; i < activeTouches.Count; i++) {
//                activeTouches[i].LateUpdate();
//            }
//        }
//        public void RemoveAllFingers() {
//            List<FingerTouch> fingerTouch = new List<FingerTouch>();
//            for (int i = activeTouches.Count - 1; i >= 0; i--) {
//                fingerTouch.Add(activeTouches[i]);
//            }
//            activeTouches.Clear();

//            for (int i = fingerTouch.Count - 1; i >= 0; i--) {
//                fingerTouch[i].Release();
//            }
//        }
//        public int IndexOfTouch(int fingerId) {
//            for (int i = 0; i < activeTouches.Count; i++) {
//                if (activeTouches[i].touch.fingerId == fingerId) {
//                    return i;
//                }
//            }
//            return -1;
//        }
//        public FingerTouch GetTouch(int fingerId) {
//            int index = IndexOfTouch(fingerId);
//            if (index >= 0) {
//                return activeTouches[index];
//            }
//            return null;
//        }

//        public void RemoveTouch(int fingerId) {
//            int index = IndexOfTouch(fingerId);
//            if (index >= 0) {
//                FingerTouch touch = activeTouches[index];
//                activeTouches.RemoveAt(index);
//                touch.Release();
//            }
//        }
//        public void RemoveFinger(Touch touch) {
//            FingerTouch fingerTouch = GetTouch(touch.fingerId);
//            if (fingerTouch != null) {
//                RemoveTouch(touch.fingerId);
//            }
//        }

//        public static Collider2D OverlapPoint(Vector2 worldPos) {
//            return Physics2D.OverlapPoint(worldPos, LayerMasks.clickableMask);
//        }
//        public void AddFingerClick(Touch touch) {
//            FingerTouch fingerTouch = new FingerTouch();
//            fingerTouch.belongingTouchControls = this;
//            fingerTouch.touch = touch;
//            fingerTouch.currentTouch = touch;

//            fingerTouch.downWorldPos = TouchToWorldPos(touch);

//            Collider2D coll = OverlapPoint(fingerTouch.downWorldPos);

//            if (coll) {
//                ClickableButton2D guiButton = coll.GetComponent<ClickableButton2D>();
//                fingerTouch.SelectButton(guiButton);
//                activeTouches.Add(fingerTouch);
//            }
//        }
//    }
//}