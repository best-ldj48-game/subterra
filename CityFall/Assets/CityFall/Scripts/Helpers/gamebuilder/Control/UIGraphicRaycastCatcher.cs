﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameBuilder.Control {
    //This script is for checking if the Canvas UI
    //Should block gameplay intractions. 
    public class UIGraphicRaycastCatcher : GraphicRaycaster {
        public static UIGraphicRaycastCatcher instance;

        /// <summary> Check if pointer is touching anything on this canvas </summary>
        public static bool PointerNotTouchingUI(int pointerID) {
            return !instance || (instance.pointerIdToNumberOfHits[pointerID] == 0);
        }
        public static bool MouseNotTouchingUI() {
            return !instance || (instance.mouseNumberOfUIHits == 0);
        }

        private int[] pointerIdToNumberOfHits;
        private int mouseNumberOfUIHits;


        protected override void Awake() {
            instance = this;
            pointerIdToNumberOfHits = new int[100];
        }


        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList) {
            base.Raycast(eventData, resultAppendList);
            //Debug.Log("second");
            if (eventData.pointerId < 0) {
                mouseNumberOfUIHits = resultAppendList.Count;
            } else if (eventData.pointerId < pointerIdToNumberOfHits.Length) {
                pointerIdToNumberOfHits[eventData.pointerId] = resultAppendList.Count;
            }
        }
    }
}