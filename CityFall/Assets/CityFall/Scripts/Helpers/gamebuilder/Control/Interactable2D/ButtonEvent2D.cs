﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace GameBuilder.Control {
    public class ButtonEvent2D : ClickableButton2D {

        public UnityEvent onClick;

        public override void Clicked() {
            base.Clicked();
            onClick.Invoke();
        }
    }
}