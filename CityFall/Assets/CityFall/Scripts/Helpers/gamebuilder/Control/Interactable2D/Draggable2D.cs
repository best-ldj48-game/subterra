﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameBuilder;

namespace GameBuilder.Control {
    public class Draggable2D : ClickableButton2D {
        public UnityEvent onDragStart;
        public UnityEvent onDragEnd;

        [HideInInspector] public Collider2D coll;

        private Vector2 mousePosAtClick;
        private Vector2 posAtClick;

        [HideInInspector] public bool held = false;


        public bool _canDrag;
        public bool canDrag {
            get {
                return _canDrag;
            }
            set {
                _canDrag = value;
                coll.enabled = canDrag;
            }
        }
        public virtual void Awake() {
            coll = GetComponent<Collider2D>();
            held = false;
            canDrag = true;
        }
        public virtual void Update() {
            if (held) {
                Vector2 offset = mouseSource.WorldPos() - mousePosAtClick;
                transform.SetXY(offset + posAtClick);
            }
        }

        public override void Clicked() {
            base.Clicked();

            if (!canDrag) {
                return;
            }

            held = true;
            coll.enabled = false;

            posAtClick = transform.position;
            mousePosAtClick = mouseSource.WorldPos();

            onDragStart.Invoke();
        }
        public override void UnSelected() {
            base.UnSelected();

            held = false;
            coll.enabled = canDrag;

            onDragEnd.Invoke();
        }
        public override Collider2D GetCollider() {
            return coll;
        }
    }
}