﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameBuilder;

namespace GameBuilder.Control {
    public class MultiTouchButton2D : ClickableButton2D {
        [HideInInspector] public Collider2D coll;
        [HideInInspector] public List<MouseSource2D> mouseSources;

        public virtual void Awake() {
            coll = GetComponent<Collider2D>();
            mouseSources = new List<MouseSource2D>();
        }

        public override void Clicked(MouseSource2D mouseSource2D) {
            base.Clicked(mouseSource2D);
            mouseSources.Add(mouseSource2D);
        }
        public override void UnSelected() {
            base.UnSelected();
        }
        public override void UnSelected(MouseSource2D mouseSource2D) {
            base.UnSelected(mouseSource2D);
            mouseSources.Remove(mouseSource2D);
        }
        public override Collider2D GetCollider() {
            return coll;
        }
    }
}