﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBuilder {
    public class FillSlider : MonoBehaviour {

        RectTransform rectTransform => transform as RectTransform;
        public RectTransform insideFill;

        [HideInInspector] public float _normFill;
        public float normFill {
            set {
                _normFill = value;
                Vector2 sizeDelta = rectTransform.sizeDelta;
                insideFill.sizeDelta = new Vector2(sizeDelta.x * value, sizeDelta.y);
            }
            get {
                return _normFill;
            }
        }
    }
}