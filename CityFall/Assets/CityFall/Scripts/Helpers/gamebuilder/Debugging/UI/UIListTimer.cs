﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GameBuilder {
    public class UIListTimer : MonoBehaviour {
        public static UIListTimer instance;
        public static void StaticNewTimer(string timerName) {
            if (instance) {
                instance.NewTimer(timerName);
            }
        }

        public TextMeshProUGUI timeTextPrefab;
        [HideInInspector] public string currentTimerText;
        [HideInInspector] public TextMeshProUGUI currentTimer;
        [HideInInspector] public System.DateTime timerStart;

        string timerText => currentTimerText + ": " + (System.DateTime.Now - timerStart).TotalMilliseconds;
        private void Awake() {
            instance = this;
        }
        private void Update() {
            if (currentTimer) {
                currentTimer.text = timerText;
            }
        }

        public void NewTimer(string timerName) {
            if (currentTimer) {
                currentTimer.text = timerText;
            }
            currentTimerText = timerName;
            currentTimer = Instantiate<GameObject>(timeTextPrefab.gameObject, timeTextPrefab.transform.parent).GetComponent<TextMeshProUGUI>();
            timerStart = System.DateTime.Now;
            currentTimer.text = timerText;
        }
    }
}