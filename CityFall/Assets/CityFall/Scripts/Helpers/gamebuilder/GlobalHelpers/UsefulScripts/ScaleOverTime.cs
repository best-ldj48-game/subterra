﻿using UnityEngine;
using System.Collections;
using GameBuilder;

namespace GameBuilder {
    public class ScaleOverTime : MonoBehaviour {

        public AnimationCurve animationCurve;
        public float length;
        public float scaleMulti;
        public float scaleOffset = 0f;

        public bool unscaledTime;
        public bool resetOnEnable;

        [HideInInspector] public float timeOfStart;
        public float time {
            get {
                return unscaledTime ? Time.unscaledTime : Time.time;
            }
        }
        public float normalizedTime {
            get {
                return (time - timeOfStart) / length;
            }
            set {
                timeOfStart = time - value * length;
                float along = animationCurve.Evaluate(value);
                transform.localScale = Vector3.one * (along * scaleMulti + scaleOffset);
            }
        }
        private void Awake() {
            timeOfStart = time;
            Update();
        }
        public void OnEnable() {
            if (resetOnEnable) {
                timeOfStart = time;
                Update();
            }
        }
        void Start() {
            timeOfStart = time;
            Update();
        }
        public void Update() {
            float along = (time - timeOfStart) / length;
            float scaleNow = animationCurve.Evaluate(along);
            transform.localScale = Vector3.one * (scaleNow * scaleMulti + scaleOffset);
        }
        public void Reset() {
            timeOfStart = time;
            Update();
        }
    }
}