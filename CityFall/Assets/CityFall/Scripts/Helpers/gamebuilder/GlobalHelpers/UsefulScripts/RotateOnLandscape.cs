﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBuilder;

public class RotateOnLandscape : MonoBehaviour {

    public bool isLandscape {
        get {
            return (Screen.width / (float)Screen.height) > 1f;
        }
    }

    bool lastOrientationIsLandscape;

    private void Start() {
        lastOrientationIsLandscape = isLandscape;
        UpdateRotation();
    }

    private void Update() {
        if (isLandscape != lastOrientationIsLandscape) {
            lastOrientationIsLandscape = isLandscape;
            UpdateRotation();
        }
    }
    public void UpdateRotation() {
        if (isLandscape) {
            transform.SetZRot(Screen.orientation == ScreenOrientation.LandscapeLeft ? 90f : -90f);
        } else {
            transform.SetZRot(0f);
        }
    }
}
