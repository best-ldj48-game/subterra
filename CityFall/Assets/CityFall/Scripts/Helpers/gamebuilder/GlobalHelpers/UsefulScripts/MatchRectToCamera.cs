﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBuilder;

namespace GameBuilder {
    public class MatchRectToCamera : MonoBehaviour {

        public Camera cam;
        public float referenceOrthoSize = 5f;
        [HideInInspector] public RectTransform rectTransform;
        public bool onlyAtStart = true;

        private void Awake() {
            rectTransform = transform as RectTransform;
        }
        private void Start() {
            rectTransform = transform as RectTransform;
            if (onlyAtStart) {
                Update();
                enabled = false;
            }
        }
        private void Update() {
            rectTransform.SetXY(cam.transform.position);
            Vector2 sizeDelta = new Vector2(referenceOrthoSize * 2f * cam.aspect, referenceOrthoSize * 2f);
            Vector2 scale = Vector2.one;// * (referenceOrthoSize / cam.orthographicSize);

            if (Mathf.Abs(transform.eulerAngles.z) > 45f) {
                Vector2 oldSizeDelta = sizeDelta;
                sizeDelta.x = oldSizeDelta.y / (oldSizeDelta.x / oldSizeDelta.y);
                sizeDelta.y = oldSizeDelta.y;
                scale *= (oldSizeDelta.x / sizeDelta.y);
            }
            rectTransform.SetXYScale(scale);
            rectTransform.sizeDelta = sizeDelta;// new Vector2(cam.orthographicSize * 2f * cam.aspect, cam.orthographicSize * 2f);
        }
    }
}