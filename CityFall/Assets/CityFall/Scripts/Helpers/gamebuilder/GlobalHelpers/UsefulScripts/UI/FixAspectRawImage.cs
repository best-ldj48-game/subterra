﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuilder {
    public class FixAspectRawImage : MonoBehaviour {

        [HideInInspector] public RawImage rawImage;
        [HideInInspector] public Texture previousTexture;

        RectTransform rectTransform => transform as RectTransform;

        private void Awake() {
            rawImage = GetComponent<RawImage>();
        }
        public void Update() {
            if (rawImage.texture != null && previousTexture != rawImage.texture) {
                previousTexture = rawImage.texture;

                Rect imageRect = rectTransform.rect;
                
                float textureAspect = (float)rawImage.texture.width / (float)rawImage.texture.height;
                float targetAspect = imageRect.width / imageRect.height;

                Rect uvRect = new Rect(0f, 0f, 1f, 1f);
                float deltaAspect = targetAspect / textureAspect;
                if (deltaAspect > 1) {
                    uvRect.height    = 1f/deltaAspect;
                    uvRect.center   = new Vector2(0.5f, 0.5f);
                } else {
                    uvRect.width   = 1f/deltaAspect;
                    uvRect.center   = new Vector2(0.5f, 0.5f);
                }
                rawImage.uvRect = uvRect;
            }
        }
    }
}
