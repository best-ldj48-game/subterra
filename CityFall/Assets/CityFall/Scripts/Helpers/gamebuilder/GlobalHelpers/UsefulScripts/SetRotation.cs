﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBuilder {
    public class SetRotation : MonoBehaviour {

        public Vector3 rotation;

        public void LateUpdate() {
            transform.eulerAngles = rotation;
        }
    }
}