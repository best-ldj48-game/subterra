﻿using UnityEngine;
using System.Collections;
using GameBuilder;

namespace GameBuilder {
    public class MoveCurveAnimation : MonoBehaviour {

        public Vector2 targetPos;
        public float time = 1f;
        public AnimationCurve curve;
        public bool relative;
        public bool clampX;
        public bool local = false;
        public bool unscaledTime = false;
        Vector3 startPos;
        Vector3 tPos;
        public float startTime;
        public bool flipX;

        bool intialFlip;

        SpriteRenderer spriteRend;
        public void SetNormalizedTime(float normTime) {
            Start();
            startTime = (unscaledTime ? Time.unscaledTime : Time.fixedTime) - time * normTime;
            Update();
        }
        bool intialized;

        public void Start() {
            if (intialized)
                return;
            intialized = true;

            spriteRend = GetComponent<SpriteRenderer>();
            if (spriteRend && flipX) {
                intialFlip = spriteRend.flipX;
            }
            startPos = local ? transform.localPosition : transform.position;

            tPos = targetPos;
            if (clampX)
                tPos.x = startPos.x;
            startTime += unscaledTime ? Time.unscaledTime : Time.fixedTime;

            if (relative)
                tPos += local ? transform.localPosition : transform.position;
            tPos.z = startPos.z;
            //		if (CameraScript.instance && gameObject == CameraScript.instance.gameObject)
            //			CameraScript.instance.enabled = false;
        }
        float previousEvalulation;

        void Update() {
            float timeAlong = ((unscaledTime ? Time.unscaledTime : Time.fixedTime) - startTime) / time;
            float curveEvaluation = curve.Evaluate(timeAlong);

            if (flipX) {
                float delta = (curveEvaluation - previousEvalulation);
                if (delta != 0f)
                    spriteRend.flipX = intialFlip ^ (delta < 0);
            }

            if (local) {
                transform.localPosition = new Vector3(Mathf.LerpUnclamped(startPos.x, tPos.x, curveEvaluation), Mathf.LerpUnclamped(startPos.y, tPos.y, curveEvaluation), tPos.z);
            } else {
                transform.position = new Vector3(Mathf.LerpUnclamped(startPos.x, tPos.x, curveEvaluation), Mathf.LerpUnclamped(startPos.y, tPos.y, curveEvaluation), tPos.z);
            }
            previousEvalulation = curveEvaluation;
        }
        public void ResetPosData() {
            startTime = -GameMath.microInfinity;

            //		Update ();

            startPos = local ? transform.localPosition : transform.position;

            tPos = targetPos;
            if (clampX)
                tPos.x = startPos.x;

            if (relative)
                tPos += local ? transform.localPosition : transform.position;
            tPos.z = startPos.z;
        }
        public void ResetTime() {
            startTime = unscaledTime ? Time.unscaledTime : Time.fixedTime;
        }
        void OnDrawGizmosSelected() {
            Vector2 target = targetPos;
            if (relative) {
                target += local ? (Vector2)transform.localPosition : (Vector2)transform.position;
            }
            if (local) {
                if (transform.parent != null)
                    target = transform.parent.TransformPoint(target);
            }
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(target, 50);//0.3f);
        }
    }
}