﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameBuilder {
    public class EventAfterTime : MonoBehaviour {

#if UNITY_EDITOR
        [Header("Optional")]
        public string description;
#endif

        public float time;
        public bool unscaledTime = false;
        public UnityEvent afterTime;

        [HideInInspector] public float timeAlong = 0f;

        float deltaTime {
            get {
                return (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
            }
        }
        private void Awake() {
            if (afterTime == null)
                afterTime = new UnityEvent();
        }
        void Update() {
            timeAlong += deltaTime;
            if (timeAlong > time) {
                afterTime.Invoke();
                Destroy(this);
            }
        }
    }
}