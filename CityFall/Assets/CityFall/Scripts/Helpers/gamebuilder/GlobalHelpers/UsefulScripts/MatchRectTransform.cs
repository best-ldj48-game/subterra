﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBuilder {
    public class MatchRectTransform : MonoBehaviour {

        public RectTransform targetRectTransform;
        [HideInInspector] public RectTransform rectTransform;

        private void Awake() {
            rectTransform = transform as RectTransform;
        }
        private void Update() {
            rectTransform.sizeDelta = targetRectTransform.sizeDelta;
        }
    }
}