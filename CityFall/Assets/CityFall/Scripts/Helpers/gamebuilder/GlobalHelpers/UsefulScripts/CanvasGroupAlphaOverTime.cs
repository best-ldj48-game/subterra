﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuilder {
    public class CanvasGroupAlphaOverTime : MonoBehaviour {

        public AnimationCurve animationCurve;
        public float length = 1f;
        public float timeScale = 1f;

        public float startNormalizedTime = 0f;

        [HideInInspector] public float along;
        [HideInInspector] public CanvasGroup canvasGroup;

        private void Awake() {
            along = startNormalizedTime;
            canvasGroup = GetComponent<CanvasGroup>();
        }
        private void Update() {
            along += Time.deltaTime / length * timeScale;
            canvasGroup.alpha = animationCurve.Evaluate(along);
        }
    }
}