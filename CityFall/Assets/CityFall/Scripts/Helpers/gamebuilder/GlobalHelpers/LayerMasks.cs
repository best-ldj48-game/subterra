﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBuilder {
    public static class LayerMasks {

        public static int guiLayerIndex = 8;

        public static int guiLayerMask = 1 << guiLayerIndex;

        public static int clickableMask = guiLayerMask;
    }
}