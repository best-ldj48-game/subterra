﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace GameBuilder {
    [System.Serializable]
    public class BooleanUnityEvent : UnityEvent<bool> {
        public void ExclusiveAddListener(UnityAction<bool> listener) {
            RemoveListener(listener);
            AddListener(listener);
        }
    }
    public static class Extentions {
        public static void SetXY(this Transform transform, float x, float y) {
            Vector3 pos = transform.position;
            pos.x = x;
            pos.y = y;
            transform.position = pos;
        }
        public static void SetXY(this Transform transform, Vector2 xy) {
            Vector3 pos = transform.position;
            pos.x = xy.x;
            pos.y = xy.y;
            transform.position = pos;
        }
        public static void SetLocalXY(this Transform transform, Vector2 xy) {
            Vector3 pos = transform.localPosition;
            pos.x = xy.x;
            pos.y = xy.y;
            transform.localPosition = pos;
        }
        public static void SetLocalZRot(this Transform transform, float zRot) {
            Vector3 rot = transform.localEulerAngles;
            rot.z = zRot;
            transform.localEulerAngles = rot;
        }
        public static void SetLocalYRot(this Transform transform, float yRot) {
            Vector3 rot = transform.localEulerAngles;
            rot.y = yRot;
            transform.localEulerAngles = rot;
        }
        public static void SetZRot(this Transform transform, float zRot) {
            Vector3 rot = transform.eulerAngles;
            rot.z = zRot;
            transform.eulerAngles = rot;
        }
        public static void SetYRot(this Transform transform, float yRot) {
            Vector3 rot = transform.eulerAngles;
            rot.y = yRot;
            transform.eulerAngles = rot;
        }
        public static void SetXScale(this Transform transform, float x) {
            Vector3 scale = transform.localScale;
            scale.x = x;
            transform.localScale = scale;
        }
        public static void SetXYScale(this Transform transform, Vector2 scale) {
            Vector3 originalScale = transform.localScale;
            originalScale.x = scale.x;
            originalScale.y = scale.y;
            transform.localScale = originalScale;
        }
        public static void SetXFlip(this Transform transform, bool flipped) {
            Vector3 scale = transform.localScale;
            scale.x = flipped ? -Mathf.Abs(scale.x) : Mathf.Abs(scale.x);
            transform.localScale = scale;
        }
        public static void SetYFlip(this Transform transform, bool flipped) {
            Vector3 scale = transform.localScale;
            scale.y = flipped ? -Mathf.Abs(scale.y) : Mathf.Abs(scale.y);
            transform.localScale = scale;
        }
        public static void SetYScale(this Transform transform, float y) {
            Vector3 scale = transform.localScale;
            scale.y = y;
            transform.localScale = scale;
        }
        public static void SetX(this Transform transform, float x) {
            Vector3 pos = transform.position;
            pos.x = x;
            transform.position = pos;
        }
        public static void SetLocalX(this Transform transform, float x) {
            Vector3 pos = transform.localPosition;
            pos.x = x;
            transform.localPosition = pos;
        }
        public static void SetLocalY(this Transform transform, float y) {
            Vector3 pos = transform.localPosition;
            pos.y = y;
            transform.localPosition = pos;
        }
        public static void SetY(this Transform transform, float y) {
            Vector3 pos = transform.position;
            pos.y = y;
            transform.position = pos;
        }
        public static void SetZ(this Transform transform, float z) {
            Vector3 pos = transform.position;
            pos.z = z;
            transform.position = pos;
        }
        public static void SetLocalZ(this Transform transform, float z) {
            Vector3 pos = transform.localPosition;
            pos.z = z;
            transform.localPosition = pos;
        }
        public static float Aspect (this Vector3 vector3) {
            return vector3.x / vector3.y;
        }
        public static float Aspect(this Vector2 vector2) {
            return vector2.x / vector2.y;
        }

        public static Transform SuperParent(this Transform transform) {
            Transform superParent = transform;
            while (superParent.parent != null) {
                superParent = superParent.parent;
            }
            return superParent;
        }
        public static void DetachFromParent(this Transform transform) {
            transform.SetParent(null);
        }
        public static void SetChildrenLayers(this Transform transform, int layer) {
            for (int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).SetSelfAndChildrenLayers(layer);
            }
        }
        public static void SetSelfAndChildrenLayers(this Transform transform, int layer) {
            transform.gameObject.layer = layer;
            for (int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).SetSelfAndChildrenLayers(layer);
            }
        }
        public static void LookAt2D(this Transform trans, Vector2 tPos, float offset = 0f) {
            Vector2 delta = tPos - (Vector2)trans.position;
            float zRot = -Mathf.Atan2(delta.x, delta.y) * Mathf.Rad2Deg + offset;
            trans.SetZRot(zRot);
        }
        public static void DestroySuperParent(this Transform transform, bool immediate = false) {
            Transform superParent = transform;
            while (superParent.parent != null) {
                superParent = superParent.parent;
            }
            if (immediate)
                MonoBehaviour.DestroyImmediate(superParent.gameObject);
            else
                MonoBehaviour.Destroy(superParent.gameObject);
        }
        public static T GetComponentInParent<T>(this MonoBehaviour mono, bool includeInactive) {
            return mono.transform.GetComponentInParent<T>(includeInactive);
        }
        public static T GetComponentInParent<T>(this Transform trans, bool includeInactive) {
            if (!includeInactive) {
                return trans.GetComponentInParent<T>();
            }
            Transform superParent = trans;
            T comp = default(T);
            while (superParent.parent != null) {
                comp = superParent.GetComponent<T>();
                if (!EqualityComparer<T>.Default.Equals(comp, default(T)))
                    break;
                superParent = superParent.parent;
            }
            return comp;
        }
        public static T[] GetComponentsInChildren<T>(this Transform trans, params string[] excludedTags) {
            List<T> components = new List<T>();
            GetComponentsInChildrenExcludingTags(trans, excludedTags, components);
            return components.ToArray();
        }
        public static T[] GetComponentsInChildrenExcludingThis<T>(this Transform trans, params string[] excludedTags) {
            List<T> components = new List<T>();
            for (int i = 0; i < trans.childCount; i++) {
                GetComponentsInChildrenExcludingTags(trans.GetChild(i), excludedTags, components);
            }
            return components.ToArray();
        }
        private static void GetComponentsInChildrenExcludingTags<T>(Transform trans, string[] excludedTags, List<T> fillArray) {
            for (int i = 0; i < excludedTags.Length; i++) {
                if (trans.CompareTag(excludedTags[i])) {
                    return;
                }
            }
            T component = trans.GetComponent<T>();
            if (component is Component && (component as Component) != null) {// !EqualityComparer<T>.Default.Equals(component, default(T)) && component != null) {
                fillArray.Add(component);
            }
            for (int i = 0; i < trans.childCount; i++) {
                GetComponentsInChildrenExcludingTags(trans.GetChild(i), excludedTags, fillArray);
            }
        }
        public static T[] GetComponentInImmediateChildren<T>(this GameObject go) {
            List<T> components = new List<T>();
            for (int i = 0; i < go.transform.childCount; i++) {
                T component = go.transform.GetChild(i).GetComponent<T>();
                if (!EqualityComparer<T>.Default.Equals(component, default(T)))
                    components.Add(component);
            }
            return components.ToArray();
        }
        public static void DestroyAllChildren(this Transform transform) {
            foreach (Transform trans in transform) {
                MonoBehaviour.Destroy(trans.gameObject);
            }
        }
        public static void SetChildrenParent(this Transform transform, Transform newParent) {
            for (int i = transform.childCount - 1; i >= 0; i--) {
                transform.GetChild(i).SetParent(newParent);
            }
        }
        public static void DestroyAllChildrenImmideate(this Transform transform) {
            while (transform.childCount != 0) {
                MonoBehaviour.DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }
        public static void SetChildrenState(this Transform transform, bool state) {
            foreach (Transform trans in transform) {
                trans.gameObject.SetActive(state);
            }
        }
        public static Bound1D GetBound1D(this SpriteRenderer spriteRend) {
            Bounds bound = spriteRend.bounds;
            return new Bound1D(bound.min.x, bound.max.x);
        }
        public static Bound1D GetBound1D(this Collider2D coll) {
            Bounds bound = coll.bounds;
            return new Bound1D(bound.min.x, bound.max.x);
        }
        public static Bound1D GetLocalBound1D(this BoxCollider2D coll) {
            Bound1D bound = Bound1D.PosWidth(coll.offset.x, coll.size.x);
            return bound;
        }
        public static Rect AsRect(this Bounds bound) {
            return new Rect(bound.min, bound.size);
        }
        public static Vector2 GetPointFromNorm(this Rect rect, Vector2 normPoint) {
            return new Vector2(rect.xMin + rect.width * normPoint.x, rect.yMin + rect.height * normPoint.y);
        }
        public static Vector2 GetNormFromPoint(this Rect rect, Vector2 point) {
            return new Vector2((point.x - rect.xMin) / rect.width, (point.y - rect.yMin) / rect.height);
        }
        public static Rect GetContainedRect (this Rect thisRect, Rect containerRect) {
            Rect returnRect = thisRect;

            Vector2 containerRectSize = containerRect.size;
            Vector2 thisRectSize = thisRect.size;

            float xSizeDelta = thisRectSize.x / containerRectSize.x;
            float ySizeDelta = thisRectSize.y / containerRectSize.y;

            if (xSizeDelta > 1f || ySizeDelta > 1f) {
                returnRect = returnRect.SizeFromCenter(returnRect.size / (xSizeDelta > ySizeDelta ? xSizeDelta : ySizeDelta));
                returnRect.center = containerRect.center;
            } else {
                float xMinDelta = containerRect.xMin - thisRect.xMin;
                float yMinDelta = containerRect.yMin - thisRect.yMin;
                float xMaxDelta = containerRect.xMax - thisRect.xMax;
                float yMaxDelta = containerRect.yMax - thisRect.yMax;

                Vector2 moveBy = Vector2.zero;
                if (xMinDelta > 0f) {
                    moveBy.x = xMinDelta;
                } else if (xMaxDelta < 0f) {
                    moveBy.x = xMaxDelta;
                }
                if (yMinDelta > 0f) {
                    moveBy.y = yMinDelta;
                } else if (yMaxDelta < 0f) {
                    moveBy.y = yMaxDelta;
                }

                returnRect.center += moveBy;
            }
            return returnRect;
        }
        public static Rect SubRectFromNormalizedRect(this Rect rect, Rect normalizedRect) {
            Rect subRect = rect;
            subRect.size *= normalizedRect.size;
            subRect.position += normalizedRect.position * rect.size;

            return subRect;
        }
        public static Rect GetNormalizedRect(this Rect rect, Rect referenceRect) {
            Rect normalizedRect = rect;
            normalizedRect.size /= referenceRect.size;
            normalizedRect.position = (normalizedRect.position - referenceRect.position) / referenceRect.size;

            return normalizedRect;
        }
        public static Rect SizeFromCenter (this Rect rect, Vector2 size) {
            Rect newRect = rect;

            Vector2 center = newRect.center;
            newRect.size = size;
            newRect.center = center;

            return newRect;
        }
        public static Rect IncludePointInRect(this Rect rect, Vector2 point) {
            Rect r = rect;

            if (r.xMin > point.x) {
                r.xMin = point.x;
            }
            if (r.xMax < point.x) {
                r.xMax = point.x;
            }
            if (r.yMin > point.y) {
                r.yMin = point.y;
            }
            if (r.yMax < point.y) {
                r.yMax = point.y;
            }
            return r;
        }
        public static Rect SharedRect (this Rect rect, Rect otherRect) {
            Vector2 min = Vector2.Max(rect.min, otherRect.min);
            Vector2 max = Vector2.Min(rect.max, otherRect.max);


            return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
        }
        public static void SetTopY(this BoxCollider2D coll, float top) {
            float bot = coll.bounds.min.y;
            float pos = coll.transform.position.y;

            float center = (top + bot) / 2f;
            float yOffset = center - pos;

            coll.offset = new Vector2(coll.offset.x, yOffset);
            coll.size = new Vector2(coll.size.x, top - bot);
        }
        public static void SetVerticleBound(this BoxCollider2D coll, Bound1D bound) {
            float bot = bound.min;
            float top = bound.max;
            float pos = coll.transform.position.y;

            float center = (top + bot) / 2f;
            float yOffset = center - pos;

            coll.offset = new Vector2(coll.offset.x, yOffset);
            coll.size = new Vector2(coll.size.x, top - bot);
        }
        public static float Width(this Sprite sprite) {
            return sprite.rect.width / sprite.pixelsPerUnit;
        }
        public static float Height(this Sprite sprite) {
            return sprite.rect.height / sprite.pixelsPerUnit;
        }
        public static Vector2 NormalizedPivot(this Sprite sprite) {
            Vector2 pivot = sprite.pivot;
            pivot.x /= sprite.texture.width;
            pivot.y /= sprite.texture.height;

            return pivot;
        }
        public static void SetLocalBound1D(this BoxCollider2D coll, Bound1D bound) {
            Vector2 size = coll.size;
            Vector2 offset = coll.offset;

            size.x = Mathf.Abs(bound.width);
            offset.x = bound.center;
            coll.size = size;
            coll.offset = offset;
        }
        public static void SetLocalBound1D(this SpriteRenderer spriteRend, Bound1D bound) {
            Vector3 localScale = spriteRend.transform.localScale;
            localScale.x = bound.width / spriteRend.sprite.Width();
            spriteRend.transform.SetLocalX(bound.center);
            spriteRend.transform.localScale = localScale;
        }
        public static void SetLocalSize(this SpriteRenderer spriteRend, Vector2 size) {
            Vector2 spriteDimensions = new Vector2(spriteRend.sprite.Width(), spriteRend.sprite.Height());


            Vector3 localScale = spriteRend.transform.localScale;
            localScale.x = size.x / spriteDimensions.x;
            localScale.y = size.y / spriteDimensions.y;

            //		localScale.x = bound.width / spriteRend.sprite.Width ();
            //		spriteRend.transform.SetLocalX (bound.center);
            spriteRend.transform.localScale = localScale;
        }
        public static void SetByRect(this SpriteRenderer rend, Rect rect) {
            Rect spriteRect = rend.sprite.rect;
            spriteRect.size /= rend.sprite.pixelsPerUnit;

            spriteRect.width *= rect.width;
            spriteRect.height *= rect.height;
            spriteRect.center = rect.center;

            Vector3 scale = spriteRect.size;
            scale.z = rend.transform.localScale.z;
            Vector2 center = spriteRect.center;

            rend.transform.localScale = scale;
            rend.transform.SetXY(center.x, center.y);
        }
        public static Bound1D LocalBound1D(this Renderer rend) {
            Bounds bound = rend.bounds;
            if (rend.transform.parent) {
                bound.size = rend.transform.parent.InverseTransformVector(bound.size);
                bound.center = rend.transform.parent.InverseTransformPoint(bound.center);
            }
            return new Bound1D(bound.min.x, bound.max.x);
        }
        public static void SetAlpha(this SpriteRenderer spriteRend, float alpha) {
            Color col = spriteRend.color;
            col.a = alpha;
            spriteRend.color = col;
        }
        public static void SetAlpha(this Text text, float alpha) {
            Color col = text.color;
            col.a = alpha;
            text.color = col;
        }
        public static Rect WorldRect(this Camera cam) {
            if (cam.orthographic) {
                Rect r = new Rect();
                r.size = new Vector2(cam.orthographicSize * cam.aspect, cam.orthographicSize) * 2f;
                r.center = cam.transform.position;
                return r;
            } else {
                return new Rect();
            }
        }
        public static float OrthographicWidth (this Camera cam) {
            return cam.orthographicSize * cam.aspect * 2f;
        }
        public static Rect InverseScaleRect(this Rect rect, Vector2 vec) {
            rect.x /= vec.x;
            rect.y /= vec.y;
            rect.height /= vec.y;
            rect.width /= vec.x;

            return rect;
        }
        public static Rect NormalizedSafeAreaRect(this Camera cam) {
            Rect safeRect = Screen.safeArea;
            Rect normalizedSafeCamRect = safeRect.InverseScaleRect(new Vector2(Screen.width, Screen.height));

            return normalizedSafeCamRect;
        }
        public static Rect SafeWorldRect(this Camera cam) {
            Rect normalizedSafeCamRect = cam.NormalizedSafeAreaRect();
            Rect camWorldRect = cam.WorldRect();
            camWorldRect.min += normalizedSafeCamRect.min * camWorldRect.size;
            Vector2 size = camWorldRect.size;
            size.x *= normalizedSafeCamRect.size.x;
            size.y *= normalizedSafeCamRect.size.y;
            camWorldRect.size = size;

            return camWorldRect;

        }
        public static string DoubleSpace(this string str) {
            return str.Replace(@" ", @"  ");
        }
        public static void DrawGizmos(this Rect rect) {
            Gizmos.DrawWireCube(rect.center, rect.size);
        }
        public static Rect TransformToWorld(this Rect rect, Transform transform) {
            return new Rect(transform.TransformPoint(rect.position), transform.TransformVector(rect.size));
        }
        public static Color BoostColor(this Color c, float amount) {
            c.r *= amount;
            c.g *= amount;
            c.b *= amount;

            return c;
        }
        public static void Shuffle<T>(this List<T> list) {
            int n = list.Count;
            while (n > 1) {
                n--;
                int k = Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        public static Vector2 NormalizedPosition(this Touch touch) {
            return new Vector2(touch.position.x / (float)Screen.width, touch.position.y / (float)Screen.height);
        }
        public static Vector2 CurrentVel(this Touch touch) {
            Touch t = Input.GetTouch(touch.fingerId);
            return t.deltaPosition / t.deltaTime;
        }
        public static void SetAnchoredY(this RectTransform rectTransform, float y) {
            Vector2 pos = rectTransform.anchoredPosition;
            pos.y = y;
            rectTransform.anchoredPosition = pos;
        }
        public static void SetAnchoredX(this RectTransform rectTransform, float X) {
            Vector2 pos = rectTransform.anchoredPosition;
            pos.x = X;
            rectTransform.anchoredPosition = pos;
        }
        public static void SetSizeDeltaY(this RectTransform rectTransform, float y) {
            Vector3 size = rectTransform.sizeDelta;
            size.y = y;
            rectTransform.sizeDelta = size;
        }
        public static Rect GetWorldRect(this Camera cam) {
            Rect r = new Rect();
            r.size = new Vector2(cam.orthographicSize * cam.aspect, cam.orthographicSize) * 2f;
            r.center = cam.transform.position;
            return r;
        }
        public static Rect GetPart(this Rect rectTransform, Rect normalizedPart) {
            Rect part = rectTransform;

            part.xMin = part.xMin + normalizedPart.xMin * rectTransform.width;
            part.yMin = part.yMin + normalizedPart.yMin * rectTransform.height;

            part.xMax = part.xMax - (1 - normalizedPart.xMax) * rectTransform.width;
            part.yMax = part.yMax - (1 - normalizedPart.yMax) * rectTransform.height;

            return part;
        }
        public static void SetLossyScale(this Transform transform, Vector3 scale) {
            Transform transformParent = transform.parent;
            transform.SetParent(null);
            transform.localScale = scale;
            transform.SetParent(transformParent);
        }
        public static void SetXYLossyScale(this Transform transform, Vector2 scale) {
            
            Transform transformParent = transform.parent;
            transform.SetParent(null);
            Vector3 newScale = scale;
            newScale.z = transform.localScale.z;
            transform.localScale = newScale;
            transform.SetParent(transformParent);
        }
        public static Transform FindDeepChild(this Transform parent, string name) {
            foreach (Transform child in parent) {
                if (child.name.Contains(name))
                    return child;
                Transform result = child.FindDeepChild(name);
                if (result != null)
                    return result;
            }
            return null;
        }
        public static void SetAlpha(this Image image, float alpha) {
            Color col = image.color;
            col.a = alpha;
            image.color = col;
        }
        public static void ExclusiveAddListener (this UnityEvent unityEvent, UnityAction listener) {
            unityEvent.RemoveListener(listener);
            unityEvent.AddListener(listener);
        }
        public static float RoundToDecimalPlaces(this float number, int decimalPlaces) {
            int multy = (int)Mathf.Pow(10, decimalPlaces);
            number *= multy;
            number = Mathf.Round(number);
            number /= multy;
            return number;
        }
        public static bool PointIsVisible(this Camera cam, Vector3 point) {
            Vector3 viewportPos = cam.WorldToViewportPoint(point);
            return viewportPos.x <= 1 && viewportPos.x >= 0 && viewportPos.y >= 0f && viewportPos.y <= 1f;
        }
        public static string AddMonoSpaceTags(this string str) {
            return "<mspace=0.6em>" + str + "</mspace>";
        }
        public static void SetRequestHeaders(this UnityWebRequest unityWebRequest, Dictionary<string, string> headers) {
            foreach (KeyValuePair<string, string> entry in headers) {
                unityWebRequest.SetRequestHeader(entry.Key, entry.Value);
            }
        }
        public static bool IsParent(this Transform transform, Transform parent) {
            Transform trans = transform.parent;
            for (int i = 0; i < 500; i++) {//An upper limit on parent search
                if (!trans)
                    return false;
                if (trans == parent) {
                    return true;
                }
                trans = trans.parent;
            }
            return false;
        }
        public static float HorizontalFOV(this Camera cam) {
            var radAngle = cam.fieldOfView * Mathf.Deg2Rad;
            var radHFOV = 2 * Mathf.Atan(Mathf.Tan(radAngle / 2) * cam.aspect);
            var hFOV = Mathf.Rad2Deg * radHFOV;

            return hFOV;
        }

        public static Vector3 Inverse(this Vector3 vector) {
            return new Vector3(1f / vector.x, 1f / vector.y, 1f / vector.z);
        }
        public static Vector2Int AsVector2 (this Vector2 vector) {
            return new Vector2Int((int)vector.x, (int)vector.y);
        }
        public static int FindFileIndexThatStartsWithInPaths(this string[] paths, string fileName) {
            for (int i = 0; i < paths.Length; i++) {
                if (Path.GetFileNameWithoutExtension(paths[i]).StartsWith(fileName)) {
                    return i;
                }
            }
            return -1;
        }
        public static string FindFileThatStartsWithInPaths(this string[] paths, string fileName) {
            int index = paths.FindFileIndexThatStartsWithInPaths(fileName);
            if (index < 0) {
                return "";
            }
            return paths[index];
        }
        public static int FindIndexOfStringThatStartsWith(this string[] thisString, string startString) {
            for (int i = 0; i < thisString.Length; i++) {
                if (thisString[i].StartsWith(startString)) {
                    return i;
                }
            }
            return -1;
        }
        public static string FindStringThatStartsWith (this string[] thisString, string startString) {
            int index = thisString.FindIndexOfStringThatStartsWith(startString);
            if (index < 0) {
                return "";
            }
            return thisString[index];
        }
    }
}