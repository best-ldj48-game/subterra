﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;

namespace GameBuilder {
    public static class HelperFunctions {

        public static Vector2 MouseNormalizedPosition() {
            Vector2 position = Input.mousePosition;
            return new Vector2(position.x / (float)Screen.width, position.y / (float)Screen.height);
        }
        public static string GetTimeString(this float time) {
            float seconds = (int)time % 60;
            float minutes = (int)time / 60;
            return minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');
        }
        public static void DeleteEverythingInDirectory(string path) {
            if (Directory.Exists(path)) {
                DirectoryInfo directory = new DirectoryInfo(path);
                foreach (FileInfo file in directory.EnumerateFiles()) {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in directory.EnumerateDirectories()) {
                    dir.Delete(true);
                }
                Directory.Delete(path);
            }
        }
        public static string LoadTextFile(string path) {
            if (File.Exists(path)) {
                return File.ReadAllText(path);
            }
            return "";
        }
        public static bool isTablet {
            get {
                float aspectRatio = (float)Screen.width / (float)Screen.height;
                if (aspectRatio > 1f) {
                    aspectRatio = 1f / aspectRatio;
                }
                return aspectRatio > 0.72f;
            }
        }
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs) {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists) {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName)) {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files) {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs) {
                foreach (DirectoryInfo subdir in dirs) {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
    public static class GameMath {
        public const float microInfinity = 999999;
        public static float Map(float value, float fromMin, float fromMax, float toMin, float toMax) {
            return Mathf.Clamp01((value - fromMin) / (fromMax - fromMin)) * (toMax - toMin) + toMin;
        }
        public static float NormalizedSmoothLerp(float normalizedTime) {
            return Mathf.Clamp01(Mathf.Sin(GameMath.Map(normalizedTime, 0f, 1f, 0f, Mathf.PI / 2f)));
        }
        public static float DistanceToFitInCameraViewX(float width) {
            float widthFOV = Camera.main.HorizontalFOV();
            return (width / 2f) / Mathf.Tan(widthFOV / 2f * Mathf.Deg2Rad);
        }

        /// <summary>
        /// Attempt to reproduce smooth slowing like on iOS 
        /// </summary>
        public static float SmoothSlow(float speed, float minSpeedForScroll, float slowdownCoeficient) {
            if (Mathf.Abs(speed) < minSpeedForScroll) {
                return 0f;
            }
            return speed * slowdownCoeficient;
        }

        public static Rect ClampRect (this Rect rect, Rect clampingRect) {
            Vector2 newCenter = rect.center;

            if (rect.width > clampingRect.width) {
                newCenter.x = clampingRect.center.x;
            } else if (rect.xMin < clampingRect.xMin) {
                newCenter.x = clampingRect.xMin + rect.width / 2f;
            } else if (rect.xMax > clampingRect.xMax) {
                newCenter.x = clampingRect.xMax - rect.width / 2f;
            }

            if (rect.height > clampingRect.height) {
                newCenter.y = clampingRect.center.y;
            } else if (rect.yMin < clampingRect.yMin) {
                newCenter.y = clampingRect.yMin + rect.height / 2f;
            } else if (rect.yMax > clampingRect.yMax) {
                newCenter.y = clampingRect.yMax - rect.height / 2f;
            }
            rect.center = newCenter;

            return rect;
        }
        public static Rect RectCenterSize(Vector2 center, Vector2 size) {
            return new Rect(center - size / 2f, size);
        }
        public static Vector2 [] ToVector2(this Vector2Int[] array) {
            Vector2[] vec2Array = new Vector2[array.Length];
            for (int i = 0; i < vec2Array.Length; i++) {
                vec2Array[i] = array[i];
            }
            return vec2Array;
        }
        public static Rect RectContainingPoints(params Vector2Int[] points) {
            return RectContainingPoints(ToVector2(points));
        }
        public static Rect RectContainingPoints(params Vector2[] points) {
            if (points.Length == 0)
                return new Rect();

            Rect rect = new Rect(points[0].x, points[0].y, 0, 0);
            for (int i = 0; i < points.Length; i++) {
                if (rect.xMin > points[i].x) {
                    rect.xMin = points[i].x;
                } else if (rect.xMax < points[i].x) {
                    rect.xMax = points[i].x;
                }

                if (rect.yMin > points[i].y) {
                    rect.yMin = points[i].y;
                } else if (rect.yMax < points[i].y) {
                    rect.yMax = points[i].y;
                }
            }
            return rect;
        }
    }
    [System.Serializable]
    public struct Bound1D {
        public float min, max;
        public float width {
            get {
                return max - min;
            }
            set {
                float center = min + (width / 2f);
                float halfWidth = value / 2f;
                min = center - halfWidth;
                max = center + halfWidth;
            }
        }
        public float center {
            get {
                return min + (width / 2f);
            }
            set {
                float halfWidth = this.width / 2f;
                min = value - halfWidth;
                max = value + halfWidth;
            }
        }
        public Bound1D(float min, float max) {
            this.min = min;
            this.max = max;
        }
        public Bound1D(Bounds bound) {
            this.min = bound.min.x;
            this.max = bound.max.x;
        }
        public Bound1D(params Bound1D[] bounds) {
            if (bounds.Length != 0) {
                this = bounds[0];
                for (int i = 1; i < bounds.Length; i++) {
                    this.min = Mathf.Min(this.min, bounds[i].min);
                    this.max = Mathf.Max(this.max, bounds[i].max);
                }
            } else {
                this.min = 0;
                this.max = 0;
            }
        }
        public Bound1D FromLocalSpace(Transform parentTransform) {
            Bound1D newBound = this;
            newBound.min = parentTransform.TransformPoint(new Vector3(min, 0)).x;
            newBound.max = parentTransform.TransformPoint(new Vector3(max, 0)).x;
            return newBound;
        }
        public Bound1D FromWorldSpace(Transform basedOn) {
            Bound1D newBound = this;
            newBound.min = basedOn.InverseTransformPoint(new Vector3(this.min, 0)).x;
            newBound.max = basedOn.InverseTransformPoint(new Vector3(this.max, 0)).x;
            return newBound;
        }
        public static Bound1D PosWidth(float pos, float width) {
            Bound1D bound1D;
            float halfWidth = width / 2f;
            bound1D.min = pos - halfWidth;
            bound1D.max = pos + halfWidth;
            return bound1D;
        }
        public Bound1D[] SplitBounds(params Bound1D[] splitBounds) {
            List<Bound1D> newBounds = new List<Bound1D>();
            newBounds.Add(this);

            foreach (Bound1D bound1D in splitBounds) {
                for (int i = newBounds.Count - 1; i >= 0; i--) {
                    if (bound1D.min < newBounds[i].max && bound1D.max > newBounds[i].min) {
                        Bound1D newBound1D = newBounds[i];
                        if (bound1D.min < newBounds[i].min) {
                            newBound1D.min = bound1D.max;
                        } else if (bound1D.max > newBounds[i].max) {
                            newBound1D.max = bound1D.min;
                        } else {
                            float oldMax = newBounds[i].max;
                            newBound1D.max = bound1D.min;
                            newBounds.Add(new Bound1D(bound1D.max, oldMax));
                        }
                        newBounds[i] = newBound1D;
                    }
                }
            }
            return newBounds.ToArray();
        }
        public Bound1D Intersection(Bound1D bound) {
            if (bound.max < min || max < bound.min) {
                return Bound1D.PosWidth((bound.center + center) / 2f, 0f);
            }
            Bound1D newBound;
            newBound.min = Mathf.Max(bound.min, min);
            newBound.max = Mathf.Min(bound.max, max);
            return newBound;
        }
        public Bound1D NormalisedIntersection(Bound1D bound) {
            float width = this.width;
            bound.min -= min;
            bound.max -= min;

            bound.min /= width;
            bound.max /= width;

            return bound;
        }
        public float NormalizedPos(float pos) {
            return GameMath.Map(pos, this.min, this.max, 0f, 1f);
        }
        public float FromNormalizedPos(float normalizedPos) {
            return GameMath.Map(normalizedPos, 0f, 1f, this.min, this.max);
        }
        public float Delta(float toPoint) {
            if (Intersects(toPoint))
                return 0f;
            else
                return (toPoint > max) ? (toPoint - max) : (toPoint - min);
        }
        public float Delta(Bound1D bound) {
            if (Intersects(bound)) {
                return 0f;
            } else {
                return (bound.max > max) ? (bound.min - max) : (min - bound.max);
            }
        }
        public float ClampInBound(float num) {
            return Mathf.Clamp(num, min, max);
        }
        public bool Intersects(Bound1D bound) {
            return this.min < bound.max && bound.min < this.max;
        }
        public bool Intersects(float point) {
            return this.min < point && point < this.max;
        }
        public bool Encapsulates(Bound1D bound) {
            return this.min < bound.min && this.max > bound.max;
        }
        public Bound1D Include(float pos) {
            Bound1D newBound = new Bound1D(this);

            if (newBound.min > pos) {
                newBound.min = pos;
            }
            if (newBound.max < pos) {
                newBound.max = pos;
            }
            return newBound;
        }
        public Collider2D CheckForObject(LayerMask l) {
            Vector2 min = new Vector2(this.min, -GameMath.microInfinity);
            Vector2 max = new Vector2(this.max, GameMath.microInfinity);
            return Physics2D.OverlapArea(min, max, l);
        }
        public Collider2D[] CheckForObjects(LayerMask l) {
            Vector2 min = new Vector2(this.min, -GameMath.microInfinity);
            Vector2 max = new Vector2(this.max, GameMath.microInfinity);
            return Physics2D.OverlapAreaAll(min, max, l);
        }
        public void GizmosDraw(float y = 0f) {
            Vector2 min = new Vector2(this.min, y);
            Vector2 max = new Vector2(this.max, y);
            Gizmos.DrawLine(max, min);
            Gizmos.DrawLine(min + Vector2.down, min + Vector2.up);
            Gizmos.DrawLine(max + Vector2.down, max + Vector2.up);
        }
        public void VerticleGizmosDraw(float x = 0f) {
            Vector2 min = new Vector2(x, this.min);
            Vector2 max = new Vector2(x, this.max);
            Gizmos.DrawLine(max, min);
            Gizmos.DrawLine(min + Vector2.left, min + Vector2.right);
            Gizmos.DrawLine(max + Vector2.left, max + Vector2.right);
        }
        public float RandomPoint() {
            return UnityEngine.Random.Range(min, max);
        }
        public void Debug() {
            UnityEngine.Debug.Log(min + ", " + max);
        }
        public float[] SplitEvenly(int numberSplits) {
            float[] splits = new float[numberSplits];
            if (numberSplits == 1) {
                splits[0] = center;
                return splits;
            }
            float distancePerWidth = width / (numberSplits - 1);

            for (int i = 0; i < splits.Length; i++) {
                splits[i] = min + distancePerWidth * i;
            }
            return splits;
        }
    }
}