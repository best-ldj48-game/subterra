﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameBuilder {
    public class RotationManager : MonoBehaviour {
        public static RotationManager _instance;
        public static RotationManager instance {
            get {
                if (!_instance) {
                    _instance = new GameObject("RotationManager").AddComponent<RotationManager>();
                    DontDestroyOnLoad(_instance.gameObject);
                }
                return _instance;
            }
        }

        public static void AddOnDeviceRotation (UnityAction action) {
            instance.onDeviceRotation.ExclusiveAddListener(action);
        }
        public static void RemoveOnDeviceRotation (UnityAction action) {
            instance.onDeviceRotation.RemoveListener(action);
        }



        public bool isLandscape {
            get {
                return (Screen.width / (float)Screen.height) > 1f;
            }
        }

        bool lastOrientationIsLandscape;
        public UnityEvent onDeviceRotation;

        private void Awake() {
            lastOrientationIsLandscape = isLandscape;
            onDeviceRotation = new UnityEvent();
        }

        private void Update() {
            if (isLandscape != lastOrientationIsLandscape) {
                lastOrientationIsLandscape = isLandscape;
                UpdateRotation();
            }
        }
        public void UpdateRotation() {
            onDeviceRotation.Invoke();
        }
    }
}