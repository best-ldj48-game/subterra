using UnityEngine;

namespace CityFall
{
    public static class TileHelper
    {
        public static Vector2Int WorldToTile(Vector3 worldPosition)
        {
            return new Vector2Int(Mathf.FloorToInt(worldPosition.x), Mathf.FloorToInt(worldPosition.y));
        }

        public static Vector3 TileToWorld(Vector2Int coords)
        {
            return new Vector3(coords.x + 0.5f, coords.y + 0.5f, 0);
        }
    }
}