namespace CityFall
{
    public interface ICanTick
    {
        bool CanTick { get; }
    }
}