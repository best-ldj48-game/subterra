using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace CityFall
{
    public static class TickHelper
    {
        public static void Tick(List<ITick> ticks)
        {
            for (var i = ticks.Count - 1; i >= 0; i--)
            {
                var tick = ticks[i];
                // TODO: hacky removal
                if (!(tick as Object))
                {
                    ticks.RemoveAt(i);
                    continue;
                }
                if (!(tick is ICanTick canTick) || canTick.CanTick) tick.Tick();
            }
        }

        public static void SortTicks(List<ITick> ticks)
        {
            // Tick order descending since tick loop is reversed
            ticks.Sort((lhs, rhs) => GetTickOrder(rhs).CompareTo(GetTickOrder(lhs)));
        }

        private static int GetTickOrder(ITick tick)
        {
            var attribute = tick.GetType().GetCustomAttribute<TickOrderAttribute>();
            return attribute?.TickOrder ?? 0;
        }
    }
}