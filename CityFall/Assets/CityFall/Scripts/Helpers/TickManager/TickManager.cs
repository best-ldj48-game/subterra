using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class TickManager : MonoBehaviour
    {
        private List<ITick> _ticks = new List<ITick>();

        public TickManagerState State { get; set; } = TickManagerState.NotTicking;

        private void Awake()
        {
            foreach (var ticker in FindObjectsOfType<Ticker>()) _ticks.Add(ticker);
            TickHelper.SortTicks(_ticks);
        }

        public void RegisterTick(ITick tick)
        {
            _ticks.Add(tick);
            TickHelper.SortTicks(_ticks);
        }

        private void Update()
        {
            if (State == TickManagerState.Ticking) TickHelper.Tick(_ticks);
        }
    }
}