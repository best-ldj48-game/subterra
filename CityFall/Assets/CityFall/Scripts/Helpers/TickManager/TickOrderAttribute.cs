using System;

namespace CityFall
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TickOrderAttribute : Attribute
    {
        public TickOrderAttribute(int tickOrder = 0)
        {
            TickOrder = tickOrder;
        }

        public int TickOrder { get; set; }
    }
}