using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CityFall
{
    public class Ticker : MonoBehaviour, ITick
    {
        private List<ITick> _ticks = new List<ITick>();

        private void Awake()
        {
            if (Singleton<TickManager>.Exists()) Singleton<TickManager>.Get().RegisterTick(this);
            _ticks.AddRange(GetComponentsInChildren<ITick>(true).Where(t => !ReferenceEquals(t, this)));
            TickHelper.SortTicks(_ticks);
        }

        public void Tick()
        {
            TickHelper.Tick(_ticks);
        }
    }
}