﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class DiggingGrid
    {
        private HashSet<Vector2Int> _markedTiles = new HashSet<Vector2Int>();

        private Dictionary<Vector2Int, Color> _markerColors = new Dictionary<Vector2Int, Color>();

        private Dictionary<Vector2Int, bool> _markerAnimationFlag = new Dictionary<Vector2Int, bool>();

        public event Action<DiggingGrid, Vector2Int> TileMarked;

        public event Action<DiggingGrid, Vector2Int> TileCleared;

        public bool IsMarkedForDigging(Vector2Int coords)
        {
            return _markedTiles.Contains(coords);
        }

        public void MarkTileForDigging(Vector2Int coords, Color markerColor, bool animate)
        {
            if (_markedTiles.Add(coords))
            {
                _markerColors.Add(coords, markerColor);
                _markerAnimationFlag.Add(coords, animate);
                TileMarked?.Invoke(this, coords);
            }
        }

        public void ClearMarkedTile(Vector2Int coords)
        {
            if (_markedTiles.Remove(coords))
            {
                _markerColors.Remove(coords);
                _markerAnimationFlag.Remove(coords);
                TileCleared?.Invoke(this, coords);
            }
        }

        public void ClearAllTiles()
        {
            foreach (var markedTile in _markedTiles) TileCleared?.Invoke(this, markedTile);
            _markedTiles.Clear();
            _markerColors.Clear();
            _markerAnimationFlag.Clear();
        }

        public bool TryGetMarkerColor(Vector2Int coords, out Color color)
        {
            return _markerColors.TryGetValue(coords, out color);
        }

        public bool IsMarkerAnimated(Vector2Int coords)
        {
            return _markerAnimationFlag.TryGetValue(coords, out var flag) && flag;
        }
    }
}