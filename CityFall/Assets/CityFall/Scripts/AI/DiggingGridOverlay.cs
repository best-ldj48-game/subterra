﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class DiggingGridOverlay : MonoBehaviour, ITick, IDisposable
    {
        [SerializeField] private SpriteRenderer _markedTilePrefab;

        private DiggingGrid _diggingGrid;

        private Dictionary<Vector2Int, SpriteRenderer> _markers = new Dictionary<Vector2Int, SpriteRenderer>();

        public void Initialize(DiggingGrid diggingGrid)
        {
            _diggingGrid = diggingGrid;
            _diggingGrid.TileMarked += OnTileMarked;
            _diggingGrid.TileCleared += OnTileCleared;
        }

        public void Tick()
        {
            var min = 0.5f;
            var max = 1;
            var f = 3;
            var t = 2 * Mathf.PI * f * Time.unscaledTime;
            var alpha = min + (1 + Mathf.Sin(t)) * 0.5f * (max - min);

            foreach (var kvp in _markers)
            {
                var coords = kvp.Key;
                var isAnimated = _diggingGrid.IsMarkerAnimated(coords);
                var marker = kvp.Value;
                var color = marker.color;
                color.a = isAnimated ? alpha : 1;
                marker.color = color;
            }
        }

        public void Dispose()
        {
            _diggingGrid.TileMarked -= OnTileMarked;
            _diggingGrid.TileCleared -= OnTileCleared;
        }

        private void OnTileMarked(DiggingGrid diggingGrid, Vector2Int coords)
        {
            Debug.Assert(!_markers.ContainsKey(coords));
            var worldPosition = new Vector3(0.5f + coords.x, 0.5f + coords.y, 0);
            var marker = Instantiate(_markedTilePrefab, worldPosition, Quaternion.identity);
            if (diggingGrid.TryGetMarkerColor(coords, out var color)) marker.color = color;
            _markers[coords] = marker;
        }

        private void OnTileCleared(DiggingGrid diggingGrid, Vector2Int coords)
        {
            if (_markers.TryGetValue(coords, out var marker))
            {
                Destroy(marker.gameObject);
                _markers.Remove(coords);
            }
        }
    }
}