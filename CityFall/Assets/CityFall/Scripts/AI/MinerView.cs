﻿using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class MinerView : MonoBehaviour, ITick
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;

        [SerializeField] private SpriteRenderer _carriedCrystalPrefab;

        [SerializeField] private SpriteRenderer _highlight;

        [SerializeField] private Transform _carriedCrystalsAnchor;

        [SerializeField] private float _carriedCrystalRadius = 0.75f;

        [SerializeField] private float _carriedCrystalRoundsPerSecond = 360;

        [SerializeField] private Sprite _idleSprite;

        [SerializeField] private Sprite[] _runSprites;

        [SerializeField] private float _runningAnimationSpeed = 1;

        private List<SpriteRenderer> _carriedCrystals = new List<SpriteRenderer>();

        public Miner Miner { get; set; }

        public bool IsHighlighted { get; set; }

        private float _runningFrame;

        public bool FlipSprite
        {
            get => _spriteRenderer.flipX;
            set => _spriteRenderer.flipX = value;
        }

        public void Tick()
        {
            if (Miner != null)
            {
                UpdateAnimations();
                UpdateCarriedCrystals();
                UpdateHighlight();
            }
        }

        private void UpdateAnimations()
        {
            if (Miner.IsMoving && Miner.IsGrounded())
            {
                _runningFrame+=Time.deltaTime * Miner.CurrentSpeed * _runningAnimationSpeed;
                _spriteRenderer.sprite = _runSprites[Mathf.FloorToInt(_runningFrame) % _runSprites.Length];
            }
            else
            {
                _spriteRenderer.sprite = _idleSprite;
            }
        }

        private void UpdateCarriedCrystals()
        {
            for (var i = _carriedCrystals.Count - 1; i >= Miner.CarriedCrystalCount; i--)
            {
                Destroy(_carriedCrystals[i].gameObject);
                _carriedCrystals.RemoveAt(i);
            }

            for (var i = _carriedCrystals.Count; i < Miner.CarriedCrystalCount; i++)
                _carriedCrystals.Add(Instantiate(_carriedCrystalPrefab, _carriedCrystalsAnchor));

            for (var i = 0; i < _carriedCrystals.Count; i++)
            {
                var crystal = _carriedCrystals[i];
                var t = 2 * Mathf.PI / _carriedCrystalRoundsPerSecond * Time.time;
                var p = (float) (i + 1) / _carriedCrystals.Count * 2 * Mathf.PI;
                var dx = Mathf.Cos(t + p);
                var dy = Mathf.Sin(t + p);
                crystal.transform.localPosition = _carriedCrystalRadius * new Vector3(dx, dy, 0);
            }
        }

        private void UpdateHighlight()
        {
            _highlight.enabled = IsHighlighted;
        }
    }
}