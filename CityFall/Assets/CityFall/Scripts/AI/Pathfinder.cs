﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class Pathfinder
    {
        private static readonly Vector2Int[] Directions = {Vector2Int.right, Vector2Int.left, Vector2Int.down};

        private List<Node> _open = new List<Node>();

        private Dictionary<Vector2Int, Node> _nodePool = new Dictionary<Vector2Int, Node>();

        public bool FindPath(
            Vector2Int origin,
            Predicate<Vector2Int> targetFunction,
            Func<Vector2Int, Vector2Int, int> costFunction,
            List<Vector2Int> path)
        {
            if (targetFunction.Invoke(origin)) return true;

            OpenNode(GetNode(origin), null);

            while (_open.Count > 0)
            {
                _open.Sort((lhs, rhs) => rhs.F.CompareTo(lhs.F));

                var currentIndex = _open.Count - 1;
                var current = _open[currentIndex];

                if (targetFunction.Invoke(current.Coords))
                {
                    BuildPath(current, path);

                    CleanUp();
                    return true;
                }

                _open.RemoveAt(currentIndex);
                current.State &= ~NodeState.Open;
                current.State |= NodeState.Closed;

                foreach (var direction in Directions)
                {
                    var neighborCoords = current.Coords + direction;

                    var cost = costFunction.Invoke(current.Coords, neighborCoords);
                    if (cost == int.MaxValue) continue;

                    var neighbor = GetNode(neighborCoords);
                    if ((neighbor.State & NodeState.Closed) != 0) continue;

                    var tentativeG = current.G + cost;
                    if ((neighbor.State & NodeState.Open) == 0)
                    {
                        neighbor.G = tentativeG;
                        neighbor.H = H(current.Coords, origin);
                        neighbor.F = neighbor.G + neighbor.H;
                        OpenNode(neighbor, current);
                    }
                    else if (tentativeG < neighbor.G)
                    {
                        neighbor.G = tentativeG;
                        neighbor.F = neighbor.G + neighbor.H;
                        neighbor.Parent = current;
                    }
                }
            }

            CleanUp();
            return false;
        }

        private void BuildPath(Node target, List<Vector2Int> path)
        {
            while (target != null)
            {
                path.Add(target.Coords);
                target = target.Parent;
            }
            path.Reverse();
        }

        private void OpenNode(Node node, Node parent)
        {
            node.State |= NodeState.Open;
            node.Parent = parent;
            _open.Add(node);
        }

        private Node GetNode(Vector2Int coords)
        {
            return _nodePool.TryGetValue(coords, out var node) ? node : PoolNode(coords);
        }

        private Node PoolNode(Vector2Int coords)
        {
            var node = new Node {Coords = coords};
            _nodePool.Add(coords, node);
            return node;
        }

        private int H(Vector2Int coords, Vector2Int target)
        {
            var dx = Mathf.Abs(coords.x - target.x);
            var dy = Mathf.Abs(coords.y - target.y);
            return dx + dy;
        }

        private void CleanUp()
        {
            _open.Clear();
            foreach (var node in _nodePool.Values) node.CleanUp();
        }

        [Flags]
        private enum NodeState
        {
            New = 0,

            Open = 1,

            Closed = 2
        }

        private class Node
        {
            public Vector2Int Coords;

            public NodeState State;

            public Node Parent;

            public int F;

            public int G;

            public int H;

            public void CleanUp()
            {
                State = NodeState.New;
                Parent = null;
                F = G = H = 0;
            }
        }
    }
}