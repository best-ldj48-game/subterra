﻿using UnityEngine;

namespace CityFall
{
    public class MinersDeath : MonoBehaviour, ITick
    {
        public void Tick()
        {
            var lavaDepth = Singleton<GameState>.Get().LavaDepth;
            var city = Singleton<City>.Get();
            for (var i = city.Miners.AwakeMiners.Count - 1; i >= 0; i--)
            {
                var miner = city.Miners.AwakeMiners[i];
                if (city.DiggableTiles.Contains(miner.Coords))
                {
                    ParticleEmitter.EmitDeathBloodParticles(miner.Coords);
                    city.Miners.KillMiner(miner);
                }
                else if(miner.Coords.y >= lavaDepth)
                {
                    ParticleEmitter.EmitDeathFireParticles(miner.Coords);
                    city.Miners.KillMiner(miner);
                }
            }
        }
    }
}