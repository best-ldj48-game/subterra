﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CityFall
{
    /// <summary>
    ///     Miner AI
    /// </summary>
    public class Miner : MonoBehaviour, ITick, ICanTick
    {
        [SerializeField] private MinerView _view;

        private City _city;

        private Terrain _terrain;

        private GameConfig.MinersConfig _config;

        private float? _wanderingDestination;

        private List<Vector2Int> _currentPath = new List<Vector2Int>();

        private int _currentPathIndex;

        private bool _facingRight;

        private float _currentSpeed;

        private float _digTimer;

        private Predicate<Vector2Int> _pathfinderTargetFunction;

        private Func<Vector2Int, Vector2Int, int> _pathfinderCostFunction;

        public bool CanTick => gameObject.activeSelf;

        public Vector2Int Coords => TileHelper.WorldToTile(transform.position);

        public float WanderingSpeed => _config.WanderingSpeed;

        private float WalkingSpeed => _config.WalkingSpeed;

        public float FallingSpeed => _config.FallingSpeed;

        public int CarriedCrystalCount { get; private set; }

        public bool IsCarryingAnyCrystals => CarriedCrystalCount > 0;

        public bool IsHighlighted
        {
            get => _view.IsHighlighted;
            set => _view.IsHighlighted = value;
        }

        public float CurrentSpeed => _currentSpeed;

        public bool IsMoving => (_currentPath.Count > 0 && _currentPathIndex < _currentPath.Count) || (_city.AreCoordsOnPlatform(Coords) && _wanderingDestination.HasValue);

        public void Initialize()
        {
            _city = Singleton<City>.Get();
            _terrain = Singleton<Terrain>.Get();
            _config = Singleton<GameConfig>.Get().Miners;
            _view.Miner = this;
        }

        public void Sleep()
        {
            gameObject.SetActive(false);
        }

        public void WakeUp()
        {
            gameObject.SetActive(true);
        }

        public void SetPath(List<Vector2Int> path)
        {
            _currentPath.Clear();
            if (path != null)
            {
                _currentPath.AddRange(path);
                AudioManager.instance.moveMiner.PlayClip();
            };
            _currentPathIndex = 0;
        }

        public void ClearPath()
        {
            SetPath(null);
        }

        public void Tick()
        {
            var isOnPlatform = _city.AreCoordsOnPlatform(Coords);
            if (isOnPlatform && CarriedCrystalCount > 0)
            {
                _city.Energy.Add(CarriedCrystalCount * Singleton<GameConfig>.Get().City.EnergyPerCrystal);
                CarriedCrystalCount = 0;
                AudioManager.instance.retrieveCrystal.PlayClip(Coords);
            }

            if (_currentPath.Count == 0)
            {
                if (isOnPlatform)
                {
                    WanderAround();
                }
                else
                {
                    if (!IsGrounded())
                        Fall();
                    else if (_config.AutoGoHome) GoHome();
                }
            }
            else
            {
                FollowPath();
            }
        }

        private void WanderAround()
        {
            _currentSpeed = WanderingSpeed;
            _wanderingDestination ??= Random.value;
            var worldDestination = _city.Miners.GetWanderingDestination(_wanderingDestination.Value);
            if (MoveTo(worldDestination)) _wanderingDestination = null;
        }

        private void FollowPath()
        {
            var target = _currentPath[_currentPathIndex];

            if (_terrain.GetTileType(target) != TileType.Empty)
            {
                _digTimer += Time.deltaTime;
                if (_digTimer < _config.DigTime) return;
                Dig(target);
                PlayFX(_currentPathIndex > 0 ? _currentPath[_currentPathIndex-1] : _currentPath[_currentPathIndex], _currentPath[_currentPathIndex]);
                _digTimer = 0;
            }

            _currentSpeed = IsGroundedWhileMoving() ? WalkingSpeed : FallingSpeed;

            if (MoveTo(TileHelper.TileToWorld(target)))
            {
                if (++_currentPathIndex == _currentPath.Count)
                    ClearPath();
                else if (_currentPath[_currentPathIndex].x != Coords.x && !IsGrounded()) Fall();
            }
        }

        private void PlayFX(Vector2Int from, Vector2Int to)
        {
            var dir = to - from;
            ParticleEmitter.EmitBreakDirtParticles(to, dir);
        }

        private void Dig(Vector2Int target)
        {
            var type = _terrain.GetTileType(target);
            _terrain.DigTile(target);
            if (type == TileType.Crystals)
            {
                CarriedCrystalCount++;
                AudioManager.instance.breakCrystal.PlayClip(Coords);
                ParticleEmitter.EmitCrystalBreakParticles(Coords);
            }
        }

        public bool IsGrounded()
        {
            return _terrain.GetTileType(Coords + Vector2Int.down) != TileType.Empty || _city.AreCoordsOnPlatform(Coords);
        }

        private bool IsGroundedWhileMoving()
        {
            var currentPathTile = _currentPath[_currentPathIndex];
            return _currentPathIndex > 0 && _currentPath[_currentPathIndex - 1].y == currentPathTile.y ||
                   _terrain.GetTileType(currentPathTile) != TileType.Empty;
        }

        private void Fall()
        {
            ClearPath();
            var current = Coords + Vector2Int.down;
            while (true)
            {
                _currentPath.Add(current);
                current += Vector2Int.down;
                if (_terrain.GetTileType(current) != TileType.Empty || _city.AreCoordsOnPlatform(current + Vector2Int.up)) break;
            }
        }

        private bool MoveTo(Vector3 destination)
        {
            var toTarget = destination - transform.position;
            var movementDelta = toTarget.normalized * CurrentSpeed * Time.deltaTime;
            var traveledDistance = movementDelta.magnitude;
            if (traveledDistance > toTarget.magnitude)
                transform.position = destination;
            else
                MoveBy(movementDelta);
            return (destination - transform.position).sqrMagnitude < 0.001f;
        }

        private void MoveBy(Vector3 delta)
        {
            transform.position += delta;
            if (!Mathf.Approximately(delta.x, 0)) _facingRight = delta.x > 0;
            _view.FlipSprite = !_facingRight;
        }

        private void GoHome()
        {
            _pathfinderTargetFunction ??= PathfinderTarget;
            _pathfinderCostFunction ??= PathfinderCost;
            Singleton<Pathfinder>.Get().FindPath(Coords, _pathfinderTargetFunction, _pathfinderCostFunction, _currentPath);
        }

        private bool PathfinderTarget(Vector2Int coords)
        {
            return _city.AreCoordsOnPlatform(coords);
        }

        private int PathfinderCost(Vector2Int coordsFrom, Vector2Int coordsTo)
        {
            if (coordsTo.y < _city.GetGroundLevel()) return int.MaxValue;
            if (coordsTo.y > coordsFrom.y) return int.MaxValue;
            var terrain = _terrain;
            if (coordsTo.y < coordsFrom.y) return terrain.GetTileType(coordsTo) == TileType.Empty ? 1 : 4;
            if (_city.AreCoordsOnPlatform(coordsTo)) return 0;
            return terrain.GetTileType(coordsFrom + Vector2Int.down) != TileType.Empty ? 1 : int.MaxValue;
        }
    }
}