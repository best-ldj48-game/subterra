﻿using UnityEngine;

namespace CityFall
{
    public class GameState
    {
        public bool Paused { get; set; }

        public bool Dead { get; set; }

        public float LavaDepth { get; set; }

        public int DepthInMeters => GetDepthInMeters();

        private int GetDepthInMeters()
        {
            var depthInUnits = Mathf.Abs(Singleton<City>.Get().GetGroundLevel());
            var cityConfig = Singleton<GameConfig>.Get().City;
            return cityConfig.InitialMeters + depthInUnits * cityConfig.UnitsToMeters;
        }
    }
}