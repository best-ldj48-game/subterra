using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class CityPopulation : CityBehaviour
    {
        private List<HousesSlot> _slots = new List<HousesSlot>();

        [SerializeField] private List<Transform> _housesSlotAnchors;

        [SerializeField] private House _housePrefab;

        public float Population { get; private set; }

        public int IntPopulation => Mathf.FloorToInt(Population);

        public List<House> Houses { get; } = new List<House>();

        public void Initialize(int initialPopulation)
        {
            foreach (var housesSlotAnchor in _housesSlotAnchors)
            {
                var slot = new HousesSlot(housesSlotAnchor);
                _slots.Add(slot);
            }
            SetPopulation(initialPopulation);
        }

        public override void Tick()
        {
            base.Tick();
            var populationGrowthRate = Singleton<GameConfig>.Get().City.PopulationGrowthRate;
            SetPopulation(Population + populationGrowthRate * Time.deltaTime);
        }

        public void SetPopulation(float population)
        {
            Population = population;
            //UpdateHouses();
        }

        private void UpdateHouses()
        {
            var citizensPerHouse = Singleton<GameConfig>.Get().City.CitizensPerHouse;
            var requiredHouses = Mathf.CeilToInt((float) IntPopulation / citizensPerHouse);
            if (requiredHouses > Houses.Count) Grow(requiredHouses - Houses.Count);
        }

        private void Grow(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var slot = GetSlotForNewHouse();
                AddHouse(slot);
            }
        }

        private HousesSlot GetSlotForNewHouse()
        {
            var suitableSlots = new List<HousesSlot>();

            var minHouseCount = int.MaxValue;
            foreach (var slot in _slots)
                if (slot.HouseCount < minHouseCount)
                {
                    minHouseCount = slot.HouseCount;
                    suitableSlots.Clear();
                    suitableSlots.Add(slot);
                }
                else if (slot.HouseCount == minHouseCount)
                {
                    suitableSlots.Add(slot);
                }
            Debug.Assert(suitableSlots.Count > 0);

            return suitableSlots[Random.Range(0, suitableSlots.Count)];
        }

        private void AddHouse(HousesSlot slot)
        {
            var house = Instantiate(_housePrefab, slot.WorldAnchor);
            house.transform.localPosition = slot.HouseCount * Vector3.up;
            slot.Houses.Add(house);
            Houses.Add(house);
        }

        private class HousesSlot
        {
            public HousesSlot(Transform worldAnchor)
            {
                WorldAnchor = worldAnchor;
            }

            public Transform WorldAnchor { get; }

            public List<House> Houses { get; } = new List<House>();

            public int HouseCount => Houses.Count;
        }
    }
}