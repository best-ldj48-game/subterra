﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace CityFall
{
    public class CityFX : CityBehaviour
    {
        [SerializeField] private ParticleSystem _smokeParticleSystem;

        [SerializeField] private ParticleSystem _crystalClaimParticles;

        [SerializeField] private Light2D _runningLight;

        [SerializeField] private int _particlesPerEnergy;

        private float? _lastEnergy;

        public override void Tick()
        {
            if (_lastEnergy.HasValue)
            {
                var diff = City.Energy.Value - _lastEnergy.Value;
                if (diff > 0)
                {
                    _crystalClaimParticles.Emit(Mathf.RoundToInt(diff * _particlesPerEnergy));
                }
            }
            
            _lastEnergy = City.Energy.Value;

            if (City.Engine.IsOn)
            {
                if (!_smokeParticleSystem.isPlaying)
                {
                    _smokeParticleSystem.Play();
                }
            }
            else
            {
                if (_smokeParticleSystem.isPlaying)
                {
                    _smokeParticleSystem.Stop();
                }
            }

            _runningLight.enabled = City.Engine.IsOn;
        }
    }
}