using System;

namespace CityFall
{
    public abstract class CityResourceStorage<T> : CityBehaviour where T : IComparable<T>
    {
        public T Value { get; private set; }

        public T Min { get; set; }

        public T Capacity { get; set; }

        public void Initialize(T value, T min, T capacity)
        {
            Min = min;
            Capacity = capacity;
            SetValue(value);
        }

        public void SetValue(T value)
        {
            Value = value.CompareTo(Min) < 0 ? Min : value.CompareTo(Capacity) > 0 ? Capacity : value;
        }

        public void Add(T value)
        {
            SetValue(Add(Value, value));
        }

        protected abstract T Add(T x, T y);
    }
}