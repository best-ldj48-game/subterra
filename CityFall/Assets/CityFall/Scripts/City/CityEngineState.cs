namespace CityFall
{
    /// <summary>
    /// City engine on / off
    /// </summary>
    public enum CityEngineState
    {
        On,

        Off
    }
}