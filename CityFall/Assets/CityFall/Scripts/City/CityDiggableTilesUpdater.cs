using UnityEngine;

namespace CityFall
{
    public class CityDiggableTilesUpdater : CityBehaviour
    {
        public const int TickOrder = 0;

        public override void Tick()
        {
            base.Tick();
            // TODO: On demand update
            UpdateDiggableTiles();
        }

        private void UpdateDiggableTiles()
        {
            City.DiggableTiles.Clear();

            var leftBoundary = City.GetLeftBoundary();
            var rightBoundary = City.GetRightBoundary();
            for (var x = leftBoundary.x; x <= rightBoundary.x; x++)
            for (var y = leftBoundary.y; y <= rightBoundary.y; y++)
                City.DiggableTiles.Add(new Vector2Int(x, y));
        }
    }
}