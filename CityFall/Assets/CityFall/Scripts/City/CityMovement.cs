using UnityEngine;

namespace CityFall
{
    [TickOrder(CityDiggableTilesUpdater.TickOrder + 1)]
    public class CityMovement : CityBehaviour
    {
        private float _digCooldown;

        public override void Tick()
        {
            base.Tick();

            Dig();
            Fall();
        }

        private void Dig()
        {
            if (!City.Engine.IsOn || City.Energy.Value <= 0)
            {
                Cooldown();
                return;
            }

            if (_digCooldown > 0)
            {
                _digCooldown -= Time.deltaTime;
                if (_digCooldown < 0) _digCooldown = 0;
                if (_digCooldown > 0) return;
            }

            var terrain = Singleton<Terrain>.Get();
            foreach (var diggable in City.DiggableTiles)
            {
                terrain.DigTile(diggable);
                ParticleEmitter.EmitBreakDirtParticles(diggable, Vector2.down);
            }
            Cooldown();
        }

        private void Cooldown()
        {
            _digCooldown = 1f / Singleton<GameConfig>.Get().City.DigSpeed;
        }

        private void Fall()
        {
            if (CanFall()) City.Fall();
        }

        private bool CanFall()
        {
            var terrain = Singleton<Terrain>.Get();
            foreach (var diggable in City.DiggableTiles)
                if (terrain.GetTileType(diggable) == TileType.Blocked)
                    return false;
            return true;
        }
    }
}