namespace CityFall
{
    public class CityEnergyStorage : CityResourceStorage<float>
    {
        protected override float Add(float x, float y)
        {
            return x + y;
        }
    }
}