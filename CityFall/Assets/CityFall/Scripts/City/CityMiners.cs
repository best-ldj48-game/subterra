using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    public class CityMiners : CityBehaviour
    {
        [SerializeField] private Miner _minerPrefab;

        [SerializeField] private Transform _minersSpawnPoint;

        [SerializeField] private Transform _wanderingLeftBoundary;

        [SerializeField] private Transform _wanderingRightBoundary;

        private float _awakeMinerCooldown;

        public List<Miner> Miners { get; } = new List<Miner>();

        public List<Miner> SleepingMiners { get; } = new List<Miner>();

        public List<Miner> AwakeMiners { get; } = new List<Miner>();

        public int DeadMinerCount { get; private set; }

        public override void Tick()
        {
            base.Tick();
            GrowMiners();
            WakeUpMiners();
        }

        private void GrowMiners()
        {
            var populationPerMiner = Singleton<GameConfig>.Get().City.PopulationPerMiner;
            var availableMiners = 1 + Mathf.FloorToInt((float) City.Population.IntPopulation / populationPerMiner) - DeadMinerCount;
            while (Miners.Count < availableMiners)
            {
                var miner = CreateMiner();
                Miners.Add(miner);
                SleepingMiners.Add(miner);
            }
        }

        private Miner CreateMiner()
        {
            var miner = Instantiate(_minerPrefab);
            miner.Initialize();
            miner.Sleep();
            return miner;
        }

        private void WakeUpMiners()
        {
            if (SleepingMiners.Count == 0)
            {
                CooldownWakingMiners();
                return;
            }

            if (_awakeMinerCooldown > 0)
            {
                _awakeMinerCooldown -= Time.deltaTime;
                if (_awakeMinerCooldown < 0) _awakeMinerCooldown = 0;
                if (_awakeMinerCooldown > 0) return;
            }

            WakeUpMiner();

            CooldownWakingMiners();
        }

        private void WakeUpMiner()
        {
            var miner = SleepingMiners[0];
            miner.transform.position = _minersSpawnPoint.position;
            miner.WakeUp();
            SleepingMiners.RemoveAt(0);
            AwakeMiners.Add(miner);
            AudioManager.instance.minerSpawn.PlayClip();
        }

        private void CooldownWakingMiners()
        {
            _awakeMinerCooldown = Singleton<GameConfig>.Get().City.AwakeMinerCooldown;
        }

        public Vector3 GetWanderingDestination(float t)
        {
            return Vector3.Lerp(_wanderingLeftBoundary.position, _wanderingRightBoundary.position, t);
        }

        public void KillMiner(Miner miner)
        {
            Miners.Remove(miner);
            AwakeMiners.Remove(miner);
            Debug.Assert(!SleepingMiners.Contains(miner));
            Destroy(miner.gameObject);
            DeadMinerCount++;
            AudioManager.instance.minerDeath.PlayClip(miner.Coords);
        }
    }
}