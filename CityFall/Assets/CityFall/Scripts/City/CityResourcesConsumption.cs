using UnityEngine;

namespace CityFall
{
    public class CityResourcesConsumption : CityBehaviour
    {
        public override void Tick()
        {
            base.Tick();
            var cityConfig = Singleton<GameConfig>.Get().City;
            var dt = Time.deltaTime;
            if (City.Engine.IsOn && City.Energy.Value > 0)
            {
                City.Energy.Add(-1 * cityConfig.EnergyConsumptionRate * dt);
            }
        }
    }
}