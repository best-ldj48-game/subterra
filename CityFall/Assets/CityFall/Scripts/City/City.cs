using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    /// <summary>
    ///     City behaviour
    /// </summary>
    public class City : MonoBehaviour
    {
        [SerializeField] private Transform _root;

        [SerializeField] private Transform _leftBoundary;

        [SerializeField] private Transform _rightBoundary;

        [SerializeField] private Transform _platformGroundLevel;

        public Vector3 LookAtTarget => _root.position + Vector3.back;

        public int Depth => Mathf.FloorToInt(_root.transform.position.y);

        public CityEngine Engine { get; private set; }

        public CityEnergyStorage Energy { get; private set; }

        public CityPopulation Population { get; private set; }

        public CityMiners Miners { get; private set; }

        public List<Vector2Int> DiggableTiles { get; } = new List<Vector2Int>();

        private void Awake()
        {
            Engine = GetComponentInChildren<CityEngine>();
            Energy = GetComponentInChildren<CityEnergyStorage>();
            Population = GetComponentInChildren<CityPopulation>();
            Miners = GetComponentInChildren<CityMiners>();
        }

        public void Initialize(GameConfig.CityConfig cityConfig)
        {
            Engine.SetState(cityConfig.InitialEngineState);
            Energy.Initialize(cityConfig.InitialEnergy, 0, cityConfig.EnergyCapacity);
            Population.Initialize(cityConfig.InitialPopulation);
        }

        public void Fall()
        {
            foreach (var miner in Miners.AwakeMiners)
                if (AreCoordsOnPlatform(miner.Coords))
                {
                    miner.ClearPath();
                    miner.transform.position -= Vector3.up;
                }
            _root.position -= Vector3.up;
        }

        public Vector2Int GetLeftBoundary()
        {
            return TileHelper.WorldToTile(_leftBoundary.position);
        }

        public Vector2Int GetRightBoundary()
        {
            return TileHelper.WorldToTile(_rightBoundary.position);
        }

        public int GetGroundLevel()
        {
            return TileHelper.WorldToTile(_platformGroundLevel.position).y;
        }

        public bool AreCoordsWithinMainTunnel(Vector2Int coords)
        {
            var left = GetLeftBoundary();
            var right = GetRightBoundary();
            return coords.x >= left.x && coords.x <= right.x;
        }

        public bool AreCoordsWithinMainTunnelAndAbovePlatform(Vector2Int coords)
        {
            return AreCoordsWithinMainTunnel(coords) && coords.y >= GetGroundLevel();
        }

        public bool AreCoordsOnPlatform(Vector2Int coords)
        {
            return AreCoordsWithinMainTunnel(coords) && coords.y == GetGroundLevel();
        }
    }
}