using System;

namespace CityFall
{
    public class CityEngine : CityBehaviour
    {
        public Action<CityEngine, CityEngineState> EngineStateChanged;

        public CityEngineState EngineState { get; private set; } = CityEngineState.Off;

        public bool IsOn => EngineState == CityEngineState.On;

        public void TurnOn()
        {
            CameraShake.StartShake();
            AudioManager.instance.machineTurnOn.PlayClip();
            SetState(CityEngineState.On);
        }

        public void TurnOff()
        {
            SetState(CityEngineState.Off);
        }

        public void Toggle()
        {
            if (EngineState == CityEngineState.Off)
                TurnOn();
            else
                TurnOff();
        }

        public void SetState(CityEngineState engineState)
        {
            if (EngineState != engineState)
            {
                EngineState = engineState;
                OnEngineStateChanged();
            }
        }

        private void OnEngineStateChanged()
        {
            EngineStateChanged?.Invoke(this, EngineState);
        }
    }
}