using UnityEngine;

namespace CityFall
{
    public class CityBehaviour : MonoBehaviour, ITick
    {
        public City City { get; private set; }

        protected virtual void Awake()
        {
            City = GetComponentInParent<City>();
        }

        public virtual void Tick()
        {
        }
    }
}