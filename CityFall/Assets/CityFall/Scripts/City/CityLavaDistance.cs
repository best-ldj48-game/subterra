namespace CityFall
{
    public class CityLavaDistance : CityResourceStorage<float>
    {
        protected override float Add(float x, float y)
        {
            return x + y;
        }
    }
}