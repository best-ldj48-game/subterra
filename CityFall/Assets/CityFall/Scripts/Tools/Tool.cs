﻿using UnityEngine;

namespace CityFall
{
    public abstract class Tool
    {
        protected Vector2Int MouseCoords
        {
            get
            {
                var mousePosition = Input.mousePosition;
                // TODO: Extract singleton
                var mouseWorld = Camera.main.ScreenToWorldPoint(mousePosition);
                return TileHelper.WorldToTile(mouseWorld);
            }
        }

        public abstract void OnToolEnabled();

        public abstract void OnToolDisabled();

        public abstract void HandlePointerDown(int button);

        public abstract void HandlePointerUp(int button);

        public abstract void HandlePointer(int button);

        public abstract void HandleTick();
    }
}