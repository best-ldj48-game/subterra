﻿using UnityEngine;

namespace CityFall
{
    public class ActiveTool : MonoBehaviour, ITick
    {
        public Tool Tool { get; private set; }

        public void SelectTool(Tool tool)
        {
            if (Tool != tool)
            {
                Tool?.OnToolDisabled();
                Tool = tool;
                Tool?.OnToolEnabled();
            }
        }

        public void CancelTool()
        {
            SelectTool(null);
        }

        public void Tick()
        {
            if (Input.GetMouseButton(1))
            {
                CancelTool();
                return;
            }

            if (Tool != null) HandlePointerEvents(0);
            Tool?.HandleTick();
        }

        private void HandlePointerEvents(int button)
        {
            if (Input.GetMouseButtonDown(button))
                Tool.HandlePointerDown(button);
            else if (Input.GetMouseButtonUp(button))
                Tool.HandlePointerUp(button);
            else if (Input.GetMouseButton(button)) Tool.HandlePointer(button);
        }
    }
}