﻿using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    /// <summary>
    ///     Marks tiles for digging
    /// </summary>
    public class DigTool : Tool
    {
        private Vector2Int? _origin;

        private List<Vector2Int> _pathPreview = new List<Vector2Int>();

        private List<Vector2Int> _committedPath = new List<Vector2Int>();

        public Miner SelectedMiner { get; set; }

        public override void OnToolEnabled()
        {
            if (SelectedMiner == null)
            {
                Singleton<ActiveTool>.Get().CancelTool();
                return;
            }

            _origin = SelectedMiner.Coords;

            var city = Singleton<City>.Get();
            if (city.AreCoordsOnPlatform(_origin.Value)) city.Engine.TurnOff();
        }

        public override void OnToolDisabled()
        {
            _origin = null;
            _pathPreview.Clear();
            _committedPath.Clear();
            SelectedMiner = null;
            Singleton<DiggingGrid>.Get().ClearAllTiles();
        }

        public override void HandlePointerDown(int button)
        {
            for (var i = _committedPath.Count == 0 ? 0 : 1; i < _pathPreview.Count; i++) _committedPath.Add(_pathPreview[i]);

            var target = _pathPreview[_pathPreview.Count - 1];
            if (_origin == target)
            {
                ConfirmPath();
            }
            else
            {
                _origin = target;
                _pathPreview.Clear();
            }
        }

        private void ConfirmPath()
        {
            SelectedMiner.SetPath(_committedPath);
            Singleton<ActiveTool>.Get().CancelTool();
        }

        public override void HandlePointerUp(int button)
        {
        }

        public override void HandlePointer(int button)
        {
        }

        public override void HandleTick()
        {
            if (_origin == null) return;

            var origin = _origin.Value;
            _pathPreview.Clear();
            _pathPreview.Add(origin);

            var target = MouseCoords;
            if (target != origin)
            {
                var dx = Mathf.Abs(origin.x - target.x);
                var dy = Mathf.Abs(origin.y - target.y);
                if (dx >= dy)
                    DoHorizontalPreview(origin, target);
                else if (!WithinMainTunnelAndAbovePlatform(origin)) DoVerticalPreview(origin, target);
            }

            for (var i = 1; i < _pathPreview.Count; i++)
            {
                var previewTile = _pathPreview[i];
                if (_committedPath.Contains(previewTile))
                {
                    _pathPreview.Clear();
                    break;
                }
            }

            UpdateDiggingGrid();
        }

        private void DoHorizontalPreview(Vector2Int origin, Vector2Int target)
        {
            while (true)
            {
                var dx = (int) Mathf.Sign(target.x - origin.x);
                var current = origin;
                target.y = origin.y;

                while (true)
                {
                    current.x += dx;
                    if (current == target || !WithinMainTunnelAndAbovePlatform(current) && EmptyAt(current + Vector2Int.down)) break;
                    _pathPreview.Add(current);
                }

                while (true)
                {
                    _pathPreview.Add(current);
                    if (WithinMainTunnelAndAbovePlatform(current) || !EmptyAt(current + Vector2Int.down)) break;
                    current.y--;
                }

                if (current.x == target.x) break;

                origin = current;
            }
        }

        private void DoVerticalPreview(Vector2Int origin, Vector2Int target)
        {
            while (true)
            {
                var dy = (int) Mathf.Sign(target.y - origin.y);
                if (dy >= 0) return;
                var current = origin;
                target.x = origin.x;

                while (true)
                {
                    current.y += dy;
                    if (current == target || EmptyAt(current + Vector2Int.down)) break;
                    _pathPreview.Add(current);
                }

                while (true)
                {
                    _pathPreview.Add(current);
                    if (!EmptyAt(current + Vector2Int.down)) break;
                    current.y--;
                }

                if (current.y <= target.y) break;

                origin = current;
            }
        }

        private bool WithinMainTunnelAndAbovePlatform(Vector2Int coords)
        {
            return Singleton<City>.Get().AreCoordsOnPlatform(coords);
        }

        private bool EmptyAt(Vector2Int coords)
        {
            return Singleton<Terrain>.Get().GetTileType(coords) == TileType.Empty;
        }

        private void UpdateDiggingGrid()
        {
            var config = Singleton<GameConfig>.Get().Markers;
            var canConfirm = _pathPreview.Count == 0 || _pathPreview[_pathPreview.Count - 1] == _origin;
            var color = canConfirm ? config.ValidPathColor : config.PreviewColor;

            var diggingGrid = Singleton<DiggingGrid>.Get();
            diggingGrid.ClearAllTiles();

            foreach (var committedTile in _committedPath) diggingGrid.MarkTileForDigging(committedTile, color, canConfirm);
            foreach (var previewTile in _pathPreview) diggingGrid.MarkTileForDigging(previewTile, color, canConfirm);
        }
    }
}