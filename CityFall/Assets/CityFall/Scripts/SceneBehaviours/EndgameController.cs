﻿using UnityEngine;

namespace CityFall
{
    public class EndgameController : MonoBehaviour, ITick
    {
        [SerializeField] private TogglePanel _endGamePanel;

        [SerializeField] private float _endGameMusicPitch;

        private void Awake()
        {
            AudioManager.instance.MusicAudioSource.pitch = 1;
        }

        public void Tick()
        {
            var gameState = Singleton<GameState>.Get();
            var city = Singleton<City>.Get();

            if (city.Depth - 1 > gameState.LavaDepth) gameState.Dead = true;

            _endGamePanel.SetVisibility(gameState.Dead ? PanelVisibility.Visible : PanelVisibility.Hidden);
            AudioManager.instance.MusicAudioSource.pitch = gameState.Dead ? _endGameMusicPitch : 1;
        }

        private void OnDestroy()
        {
            if (AudioManager.instance) AudioManager.instance.MusicAudioSource.pitch = 1;
        }
    }
}