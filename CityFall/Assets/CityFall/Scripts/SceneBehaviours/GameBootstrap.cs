using System.Collections;
using UnityEngine;

namespace CityFall
{
    /// <summary>
    ///     Game scene entry point
    /// </summary>
    public class GameBootstrap : MonoBehaviour
    {
        [SerializeField] private GameInitialization _gameInitialization;

        private IEnumerator Start()
        {
            yield return StartCoroutine(_gameInitialization.InitializeGame());
        }
    }
}