using UnityEngine;

namespace CityFall
{
    public class GameShutDown : MonoBehaviour
    {
        private void OnDestroy()
        {
            if (Singleton<DiggingGridOverlay>.TryGet(out var diggingGridOverlay)) diggingGridOverlay.Dispose();
        }
    }
}