using System;
using System.Collections;
using GameBuilder;
using UnityEngine;

namespace CityFall
{
    /// <summary>
    ///     Game init
    /// </summary>
    public class GameInitialization : MonoBehaviour
    {
        [SerializeField] private TogglePanel _loadingPanel;

        [SerializeField] private TickManager _tickManager;

        [SerializeField] private City _cityPrefab;

        [SerializeField] private Transform _citySpawnPoint;

        [SerializeField] private Transform _actorsParent;

        [SerializeField] private ActiveTool _activeTool;

        [SerializeField] private DiggingGridOverlay _diggingGridOverlay;

        public IEnumerator InitializeGame()
        {
            AudioManager.instance.PlayMusic(AudioManager.instance.gameMusicClip);
            _loadingPanel.SetVisibility(PanelVisibility.Visible);

            _tickManager.State = TickManagerState.NotTicking;
            Singleton<TickManager>.Set(_tickManager);

            var loadGameConfigRequest = Resources.LoadAsync<GameConfig>("GameConfig");
            yield return loadGameConfigRequest;
            var gameConfig = (GameConfig) loadGameConfigRequest.asset;
            Singleton<GameConfig>.Set(gameConfig);

            var terrainSeed = 0xFFFF + Environment.TickCount % 0xFFFF;
            var terrain = new Terrain(gameConfig.Terrain, terrainSeed);
            Singleton<Terrain>.Set(terrain);

            var city = Instantiate(_cityPrefab, _citySpawnPoint.position, Quaternion.identity, _actorsParent);
            city.Initialize(gameConfig.City);
            Singleton<City>.Set(city);
            yield return null;

            var timeState = new TimeState();
            timeState.Year = gameConfig.Time.InitialYear;
            Singleton<TimeState>.Set(timeState);

            var gameState = new GameState();
            Singleton<GameState>.Set(gameState);

            InitializeWorld(gameState, gameConfig);
            Camera.main.transform.SetXY((gameState.LavaDepth + Singleton<GameConfig>.Get().Camera.IntroDepthOffset) * Vector3.up);

            InitializeTools();

            InitializeAi();

            _tickManager.State = TickManagerState.Ticking;

            _loadingPanel.SetVisibility(PanelVisibility.Hidden);
        }

        private void InitializeWorld(GameState gameState, GameConfig config)
        {
            gameState.LavaDepth = config.World.InitialLavalDepth;
        }

        private void InitializeTools()
        {
            Singleton<DigTool>.Set(new DigTool());
            Singleton<ActiveTool>.Set(_activeTool);
        }

        private void InitializeAi()
        {
            var diggingGrid = new DiggingGrid();
            Singleton<DiggingGrid>.Set(diggingGrid);
            _diggingGridOverlay.Initialize(diggingGrid);

            Singleton<Pathfinder>.Set(new Pathfinder());
        }
    }
}