﻿using UnityEngine;

namespace CityFall
{
    public class LavaUpdater : MonoBehaviour, ITick
    {
        [SerializeField] private Transform _lava;

        public void Tick()
        {
            var worldConfig = Singleton<GameConfig>.Get().World;
            var elapsedTime = Singleton<TimeState>.Get().RealTime;
            var lavaSpeed = Mathf.Min(worldConfig.MinLavaSpeed + elapsedTime * worldConfig.LavaAcceleration, worldConfig.MaxLavaSpeed);

            var gameState = Singleton<GameState>.Get();
            gameState.LavaDepth -= Time.deltaTime * lavaSpeed;
            _lava.transform.position = gameState.LavaDepth * Vector3.up;
        }
    }
}