using System;
using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    [CreateAssetMenu(menuName = "City Fall/Game Config", fileName = "GameConfig")]
    public class GameConfig : ScriptableObject
    {
        public CameraConfig Camera;

        public CityConfig City;

        public MarkersConfig Markers;

        public MinersConfig Miners;

        public TerrainConfig Terrain;

        public TimeConfig Time;

        public WorldConfig World;

        [Serializable]
        public class CameraConfig
        {
            public float IntroDepthOffset = 10;

            public float IntroSpeed = 10;

            public float PanSpeed = 40;

            public float LookAtSpeed = 4;
        }

        [Serializable]
        public class CityConfig
        {
            public CityEngineState InitialEngineState = CityEngineState.On;

            public float DigSpeed = 2.5f;

            public float InitialEnergy = 100;

            public float EnergyCapacity = 100;

            public float EnergyConsumptionRate = 1;

            public int EnergyPerCrystal = 5;

            public int InitialPopulation = 10;

            // City expansion mechanic removed but code still in
            [HideInInspector] public int CitizensPerHouse = 4;

            public float PopulationGrowthRate = 0.1f;

            public int PopulationPerMiner = 100;

            public float AwakeMinerCooldown = 1;

            public int InitialMeters = 0;

            public int UnitsToMeters = 100;
        }

        [Serializable]
        public class MarkersConfig
        {
            public Color PreviewColor = Color.yellow;

            public Color ValidPathColor = Color.green;
        }

        [Serializable]
        public class MinersConfig
        {
            public float SelectionRadius = 0.5f;

            public float WanderingSpeed = 0.5f;

            public float WalkingSpeed = 2;

            public float FallingSpeed = 5;

            public float DigTime = 0.2f;

            public bool AutoGoHome = true;
        }

        [Serializable]
        public class TerrainConfig
        {
            public Vector2Int HorizontalBoundaries = new Vector2Int(-8, 8);

            public int VerticalBoundary;

            public TileGenerationConfig Caves;

            public TileGenerationConfig Crystals;

            public float CrystalsOffset = 0.75f;

            public float MinCrystalsDensity = 0.2f;

            public float CrystalsHiatusPeriod = 10;

            [Serializable]
            public class TileGenerationConfig
            {
                public List<LayerConfig> Layers;

                public float Exponent = 0.4f;

                public float Density = 0.85f;
            }

            [Serializable]
            public class LayerConfig
            {
                public float Amplitude;

                public float Frequency;

                public float LayerExponent;
            }
        }

        [Serializable]
        public class TimeConfig
        {
            public float InitialYear = 1;

            public float TimeRatio = 15;
        }

        [Serializable]
        public class WorldConfig
        {
            [Tooltip("Initial depth of the lava (should be a positive number)")]
            public float InitialLavalDepth = 10;

            [Tooltip("Initial lava speed. Tiles per second")]
            public float MinLavaSpeed = 0;

            [Tooltip("Acceleration in tiles per second")]
            public float LavaAcceleration = 0.01f;

            [Tooltip("Maximum laval speed. Tiles per second")]
            public float MaxLavaSpeed = 1;
        }
    }
}