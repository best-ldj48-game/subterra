using UnityEngine;

namespace CityFall
{
    public class ParticleEmitter : MonoBehaviour
    {
        private static ParticleEmitter instance;

        public ParticleSystem breakDirtParticleSystem;

        public ParticleSystem deathParticleBloodSystem;

        public ParticleSystem deathParticleFireSystem;

        public ParticleSystem crystalBreakParticlesSystem;

        public static void EmitBreakDirtParticles(Vector2 pos, Vector2 hitDirection)
        {
            if (instance)
            {
                instance.breakDirtParticleSystem.transform.position = pos;
                instance.breakDirtParticleSystem.transform.rotation = Quaternion.LookRotation(Vector3.forward, -hitDirection);
                instance.breakDirtParticleSystem.Emit(10);
            }
        }

        public static void EmitDeathBloodParticles(Vector2 pos)
        {
            if (instance)
                instance.deathParticleBloodSystem.Emit(
                    new ParticleSystem.EmitParams
                    {
                        position = pos,
                        applyShapeToPosition = true
                    },
                    10);
        }

        public static void EmitDeathFireParticles(Vector2 pos)
        {
            if (instance)
                instance.deathParticleFireSystem.Emit(
                    new ParticleSystem.EmitParams
                    {
                        position = pos,
                        applyShapeToPosition = true
                    },
                    1);
        }

        public static void EmitCrystalBreakParticles(Vector2 pos)
        {
            if (instance)
                instance.crystalBreakParticlesSystem.Emit(
                    new ParticleSystem.EmitParams
                    {
                        position = pos,
                        applyShapeToPosition = true
                    },
                    20);
        }

        private void Awake()
        {
            instance = this;
        }
    }
}