using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private static CameraShake instance;

    public AnimationCurve shakePowerCurve;

    private float timeOfLastShakeStart;

    private Vector3 startPos;

    public static void StartShake()
    {
        if (instance) instance.timeOfLastShakeStart = Time.unscaledTime;
    }

    private void Awake()
    {
        instance = this;
        startPos = transform.position;
        timeOfLastShakeStart = -100;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.U))
        {
            StartShake();
            AudioManager.instance.machineTurnOn.PlayClip(transform.position);
        }
#endif
        var shakeDelta = (Vector2) Random.insideUnitSphere * shakePowerCurve.Evaluate(Time.unscaledTime - timeOfLastShakeStart);
        transform.position = startPos + (Vector3) shakeDelta;
    }
}