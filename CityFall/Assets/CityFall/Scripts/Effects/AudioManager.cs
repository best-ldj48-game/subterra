using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    public const float audioZPos = -5f;

    private const string MusicVolumeKey = "MusicVolume";

    private const string SfxVolumeKey = "SfxVolume";

    private static AudioManager _instance;

    public AudioPiece destroyBlocksMine;

    public AudioPiece machineTurnOn;

    public AudioPiece uiClick;

    public AudioPiece monsterDeath;

    public AudioPiece minerDeath;

    public AudioPiece minerAttack;

    public AudioPiece moveMiner;

    public AudioPiece minerSpawn;

    public AudioPiece monsterAttack;

    public AudioPiece placePath;

    public AudioPiece startButton;

    public AudioPiece retrieveCrystal;

    public AudioPiece breakCrystal;

    public AudioClip mainMenuMusicClip;

    public AudioClip gameMusicClip;

    [Range(0, 1)] public float musicDefaultVolume = 1;

    public AudioMixer mixer;

    private AudioPiece[] allAudioPieces;

    public static AudioManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = Instantiate(Resources.Load<GameObject>("Effects/AudioManager")).GetComponent<AudioManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    public AudioSource MusicAudioSource { get; set; }

    public float MusicVolume
    {
        get
        {
            if (PlayerPrefs.HasKey(MusicVolumeKey)) return PlayerPrefs.GetFloat(MusicVolumeKey);

            return 1;
        }
        set
        {
            mixer.SetFloat(MusicVolumeKey, Mathf.Log10(Mathf.Max(value, 0.0001f)) * 20);
            PlayerPrefs.SetFloat(MusicVolumeKey, value);
        }
    }

    public float SfxVolume
    {
        get
        {
            if (PlayerPrefs.HasKey(SfxVolumeKey)) return PlayerPrefs.GetFloat(SfxVolumeKey);

            return 1;
        }
        set
        {
            mixer.SetFloat(SfxVolumeKey, Mathf.Log10(Mathf.Max(value, 0.0001f)) * 20);
            PlayerPrefs.SetFloat(SfxVolumeKey, value);
        }
    }

    public void PlayMusic(AudioClip clip)
    {
        if (MusicAudioSource.clip != clip)
        {
            MusicAudioSource.clip = clip;
            MusicAudioSource.Play();
        }
    }

    private void Awake()
    {
        _instance = this;

        allAudioPieces = new[]
        {
            destroyBlocksMine,
            machineTurnOn,
            uiClick,
            monsterDeath,
            minerDeath,
            minerAttack,
            monsterAttack,
            placePath,
            startButton,
            retrieveCrystal,
            moveMiner,
            minerSpawn,
            breakCrystal
        };

        foreach (var audioPiece in allAudioPieces) audioPiece.Intialise();

        var musicAudioSourceGo = new GameObject("Music Audio Source", typeof(AudioSource));
        musicAudioSourceGo.transform.SetParent(_instance.transform);
        MusicAudioSource = musicAudioSourceGo.GetComponent<AudioSource>();
        MusicAudioSource.outputAudioMixerGroup = mixer.FindMatchingGroups("Master/Music").First();
        MusicAudioSource.loop = true;
        MusicAudioSource.volume = musicDefaultVolume;
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey(MusicVolumeKey)) MusicVolume = PlayerPrefs.GetFloat(MusicVolumeKey);

        if (PlayerPrefs.HasKey(SfxVolumeKey)) SfxVolume = PlayerPrefs.GetFloat(SfxVolumeKey);
    }

    [Serializable]
    public class AudioPiece
    {
        public AudioClip[] clips;

        [Range(0, 1)] public float volumeMulti = 1;

        [Range(0, 2)] public float pitchMulti = 1;

        [Range(1, 5)] public int maxSimultaneous = 1;

        private AudioSource[] audioSources;

        private int nextAudioSource;

        public void Intialise()
        {
            audioSources = new AudioSource[maxSimultaneous];
            for (var i = 0; i < audioSources.Length; i++)
            {
                audioSources[i] = new GameObject("AS", typeof(AudioSource)).GetComponent<AudioSource>();
                audioSources[i].outputAudioMixerGroup = instance.mixer.FindMatchingGroups("Master/SFX").First();
                audioSources[i].playOnAwake = false;
                audioSources[i].loop = false;
                audioSources[i].volume = volumeMulti;
                audioSources[i].pitch = pitchMulti;
                audioSources[i].transform.SetParent(_instance.transform);
            }
        }

        public void PlayClip()
        {
            PlayClip(Camera.main.transform.position);
        }

        public void PlayClip(Vector2 pos)
        {
            if (clips.Length > 0)
            {
                audioSources[nextAudioSource].clip = clips[Random.Range(0, clips.Length)];
                audioSources[nextAudioSource].transform.position = new Vector3(pos.x, pos.y, audioZPos);
                audioSources[nextAudioSource].Play();

                nextAudioSource++;
                if (nextAudioSource >= audioSources.Length) nextAudioSource = 0;
            }
        }
    }
}