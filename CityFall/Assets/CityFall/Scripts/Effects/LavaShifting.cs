using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBuilder;

public class LavaShifting : MonoBehaviour {

	public AnimationCurve shiftPace;
	public float shiftSpeedMulti = 1f;
	public float shiftScale = 1f;

	[HideInInspector] public float shiftOffset;

	private void Start() {
		shiftOffset = Random.Range(0f, 1000f);
	}
	private void Update() {
		transform.SetLocalX(shiftPace.Evaluate(Time.time * shiftSpeedMulti + shiftOffset) * shiftScale);
	}
}
