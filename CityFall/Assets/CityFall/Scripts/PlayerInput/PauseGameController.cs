﻿using System;
using UnityEngine;

namespace CityFall
{
    public class PauseGameController : MonoBehaviour, ITick
    {
        [SerializeField] private TogglePanel _pauseMenu;

        public void Tick()
        {
            var gameState = Singleton<GameState>.Get();

            if (Input.GetButtonDown("PauseGame"))
            {
                gameState.Paused = !gameState.Paused;
            }

            _pauseMenu.SetVisibility(gameState.Paused ? PanelVisibility.Visible : PanelVisibility.Hidden);
        }
    }
}