﻿using UnityEngine;

namespace CityFall
{
    public class MinerSelectionController : MonoBehaviour, ITick
    {
        [SerializeField] private Camera _camera;

        public void Tick()
        {
            Miner highlightedMiner = null;

            var mouseWorldPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            var selectionRadius = Singleton<GameConfig>.Get().Miners.SelectionRadius;
            foreach (var miner in Singleton<City>.Get().Miners.AwakeMiners)
            {
                var pointerDelta = mouseWorldPosition - miner.transform.position;
                var dx = Mathf.Abs(pointerDelta.x);
                var dy = Mathf.Abs(pointerDelta.y);
                var shouldHighlight = highlightedMiner == null && dx <= selectionRadius && dy <= selectionRadius;
                if (shouldHighlight) highlightedMiner = miner;
                miner.IsHighlighted = shouldHighlight;
            }

            if (highlightedMiner != null && Input.GetMouseButton(0))
            {
                var digTool = Singleton<DigTool>.Get();
                digTool.SelectedMiner = highlightedMiner;
                Singleton<ActiveTool>.Get().SelectTool(digTool);
            }
        }
    }
}