using GameBuilder;
using UnityEngine;

namespace CityFall
{
    public class CameraController : MonoBehaviour, ITick
    {
        [SerializeField] private Camera _camera;

        [SerializeField] private Vector2Int _cameraVerticalBounds;
        [SerializeField] private Vector2Int _cameraHorizontalBounds;

        private Vector2 _panDelta;

        private bool _lockCamera;

        private bool? _engineOffOldFrame;

        private bool _playingIntro = true;

        public void Tick()
        {
            if (Singleton<GameState>.Get().Paused)
            {
                _panDelta = Vector2.zero;
                return;
            }

            if (_playingIntro)
            {
                DoIntro();
                return;
            }

            var city = Singleton<City>.Get();
            _engineOffOldFrame ??= true;
            if (_engineOffOldFrame.Value && city.Engine.IsOn) _lockCamera = true;
            _engineOffOldFrame = !city.Engine.IsOn;

            var dx = Input.GetAxisRaw("Horizontal");
            var dy = Input.GetAxisRaw("Vertical");
            _lockCamera &= Mathf.Approximately(dx, 0) && Mathf.Approximately(dy, 0);

            var cameraPosition = _camera.transform.position;
            var cityPosition = city.LookAtTarget;

            if (_lockCamera && Singleton<ActiveTool>.Get().Tool == null)
            {
                _panDelta = Vector2.zero;

                var toTarget = cityPosition - cameraPosition;
                var movement = toTarget * Singleton<GameConfig>.Get().Camera.LookAtSpeed * Time.unscaledDeltaTime;
                if (movement.magnitude > toTarget.magnitude) movement = toTarget;
                cameraPosition += movement;
            }
            else
            {
                var panInput = new Vector3(dx, dy, 0);
                var panTarget = panInput * Singleton<GameConfig>.Get().Camera.PanSpeed;
                _panDelta = Vector2.Lerp(_panDelta, panTarget, 10 * Time.unscaledDeltaTime);
                cameraPosition += (Vector3) _panDelta * Time.unscaledDeltaTime;
            }

            cameraPosition.x = Mathf.Max(cityPosition.x - _cameraHorizontalBounds.x, cameraPosition.x);
            cameraPosition.x = Mathf.Min(cityPosition.x + _cameraHorizontalBounds.y, cameraPosition.x);
            cameraPosition.y = Mathf.Max(cityPosition.y - _cameraVerticalBounds.x, cameraPosition.y);
            cameraPosition.y = Mathf.Min(cityPosition.y + _cameraVerticalBounds.y, cameraPosition.y);

            _camera.transform.SetXY(cameraPosition);
        }

        private void DoIntro()
        {
            var city = Singleton<City>.Get();
            var toTarget = city.LookAtTarget - _camera.transform.position;
            var movement = toTarget.normalized * Singleton<GameConfig>.Get().Camera.IntroSpeed * Time.unscaledDeltaTime;
            if (movement.magnitude > toTarget.magnitude) movement = toTarget;
            _camera.transform.position += movement;
            if ((_camera.transform.position - city.LookAtTarget).sqrMagnitude < 0.01f) _playingIntro = false;
        }
    }
}