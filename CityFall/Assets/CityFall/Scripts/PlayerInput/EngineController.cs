using UnityEngine;

namespace CityFall
{
    public class EngineController : MonoBehaviour, ITick
    {
        public void Tick()
        {
            if (Input.GetKeyDown(KeyCode.Space)) Singleton<City>.Get().Engine.Toggle();
        }
    }
}