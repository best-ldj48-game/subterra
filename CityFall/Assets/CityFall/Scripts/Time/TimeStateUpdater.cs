﻿using UnityEngine;

namespace CityFall
{
    public class TimeStateUpdater : MonoBehaviour, ITick
    {
        public void Tick()
        {
            var timeState = Singleton<TimeState>.Get();
            var gameState = Singleton<GameState>.Get();
            var timeRatio = Singleton<GameConfig>.Get().Time.TimeRatio;
            timeState.Year += 1f / timeRatio * Time.deltaTime;
            timeState.RealTime += Time.deltaTime;
            Time.timeScale = gameState.Paused || gameState.Dead || Singleton<ActiveTool>.Get().Tool != null ? 0 : 1;
        }

        private void OnDestroy()
        {
            // Reset from any paused states.
            Time.timeScale = 1;
        }
    }
}