﻿using UnityEngine;

namespace CityFall
{
    public class TimeState
    {
        /// <summary>
        /// Real time in seconds passed since the game start
        /// </summary>
        /// <remarks>Affected by pause</remarks>
        public float RealTime { get; set; }

        public float Year { get; set; }

        public int IntYear => Mathf.FloorToInt(Year);
    }
}