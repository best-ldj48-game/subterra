namespace CityFall
{
    /// <summary>
    ///     Tile type
    /// </summary>
    public enum TileType
    {
        Empty,

        Blocked,

        Crystals
    }
}