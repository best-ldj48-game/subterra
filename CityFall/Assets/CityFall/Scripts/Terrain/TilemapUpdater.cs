using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace CityFall
{
    /// <summary>
    ///     Unity tilemap update
    /// </summary>
    public class TilemapUpdater : MonoBehaviour, ITick
    {
        [SerializeField] private Camera _camera;

        [SerializeField] private Tilemap _tilemap;

        [SerializeField] private TileBase _dirtTile;

        [SerializeField] private TileBase _emptyTile;

        [SerializeField] private TileBase _crystalTile;

        private Vector3[] _corners = new Vector3[4];

        private List<Vector2Int> _dirtyTiles = new List<Vector2Int>();

        private void UpdateTilemap()
        {
            _camera.CalculateFrustumCorners(_camera.rect, _camera.farClipPlane, Camera.MonoOrStereoscopicEye.Mono, _corners);

            var origin = _camera.transform.position;
            var min = new Vector2Int(Mathf.FloorToInt(origin.x + _corners[0].x), Mathf.FloorToInt(origin.y + _corners[0].y)) - Vector2Int.one;
            var max = new Vector2Int(Mathf.CeilToInt(origin.x + _corners[2].x), Mathf.CeilToInt(origin.y + _corners[2].y)) + Vector2Int.one;

            foreach (var coords in _dirtyTiles) _tilemap.SetTile((Vector3Int) coords, null);
            _dirtyTiles.Clear();

            var terrain = Singleton<Terrain>.Get();
            for (var x = min.x; x <= max.x; x++)
            for (var y = min.y; y <= max.y; y++)
            {
                var coords = new Vector3Int(x, y, 0);
                var tileType = terrain.GetTileType((Vector2Int)coords);
                var tile = GetTileForType(tileType);
                coords.z = tileType == TileType.Empty ? 10 : 0;
                _tilemap.SetTile(coords, tile);
                _dirtyTiles.Add((Vector2Int)coords);
            }
        }

        private TileBase GetTileForType(TileType tileType)
        {
            switch (tileType)
            {
                case TileType.Blocked:
                    return _dirtTile;
                case TileType.Crystals:
                    return _crystalTile;
                case TileType.Empty:
                    return _emptyTile;
            }
            return null;
        }

        public void Tick()
        {
            // TODO: On demand update
            UpdateTilemap();
        }
    }
}