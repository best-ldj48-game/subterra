using System.Collections.Generic;
using UnityEngine;

namespace CityFall
{
    /// <summary>
    ///     Terrain model
    /// </summary>
    public class Terrain
    {
        private HashSet<Vector2Int> _dugTiles = new HashSet<Vector2Int>();

        private GameConfig.TerrainConfig _terrainConfig;

        private int _seed;

        public Terrain(GameConfig.TerrainConfig terrainConfig, int seed)
        {
            _terrainConfig = terrainConfig;
            _seed = seed;
        }

        public TileType GetTileType(Vector2Int coords)
        {
            if (_dugTiles.Contains(coords)) return TileType.Empty;

            if (coords.x >= _terrainConfig.HorizontalBoundaries.x && coords.x <= _terrainConfig.HorizontalBoundaries.y)
                return coords.y >= _terrainConfig.VerticalBoundary ? TileType.Empty : TileType.Blocked;

            if (N(coords, _terrainConfig.Caves) > _terrainConfig.Caves.Density) return TileType.Empty;

            var p = Mathf.PI * _terrainConfig.CrystalsOffset;
            var densityModifier = Mathf.Max(
                Mathf.Sin(p + 2 * Mathf.PI / (2 * _terrainConfig.CrystalsHiatusPeriod) * coords.y),
                _terrainConfig.MinCrystalsDensity);
            return N(coords, _terrainConfig.Crystals) > densityModifier * _terrainConfig.Crystals.Density ? TileType.Blocked : TileType.Crystals;
        }

        private float N(Vector2Int coords, GameConfig.TerrainConfig.TileGenerationConfig config)
        {
            var t = 0f;
            foreach (var layer in config.Layers)
            {
                var nx = _seed + coords.x * layer.Frequency;
                var ny = _seed + coords.y * layer.Frequency;
                var n = layer.Amplitude * Mathf.PerlinNoise(nx, ny);
                n = Mathf.Pow(n, layer.LayerExponent);
                t = Mathf.Max(t, n);
            }
            return Mathf.Pow(t, config.Exponent);
        }

        public void DigTile(Vector2Int coords)
        {
            _dugTiles.Add(coords);
            AudioManager.instance.destroyBlocksMine.PlayClip();
        }
    }
}