﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CityFall
{
    public class TutorialUiController : MonoBehaviour
    {
        private void Awake()
        {
            AudioManager.instance.PlayMusic(AudioManager.instance.mainMenuMusicClip);
        }

        private void Update()
        {
            if (Input.GetButtonDown("Skip"))
            {
                SceneManager.LoadScene(SceneHelper.GameScene);
            }
        }
    }
}