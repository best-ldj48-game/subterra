﻿using UnityEngine;

namespace CityFall
{
    public class SelectableUiController : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;

        public bool IsEnabled
        {
            get => _canvasGroup.interactable;
            set => _canvasGroup.interactable = value;
        }
    }
}