﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CityFall
{
    [RequireComponent(typeof(Button))]
    public class ButtonAudio : MonoBehaviour
    {
        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(OnButtonClick );
        }

        private void OnButtonClick()
        {
            AudioManager.instance.uiClick.PlayClip();
        }
    }
}