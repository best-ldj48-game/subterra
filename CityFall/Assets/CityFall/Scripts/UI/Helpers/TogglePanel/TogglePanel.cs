using UnityEngine;

namespace CityFall
{
    /// <summary>
    ///     Abstraction of UI panel visibility state (visible / hidden)
    /// </summary>
    public class TogglePanel : MonoBehaviour
    {
        [SerializeField] private PanelVisibility _visibility = PanelVisibility.Hidden;

        public PanelVisibility Visibility => _visibility;

        public bool IsVisible => _visibility == PanelVisibility.Visible;

        private void Awake()
        {
            OnVisibilityChanged();
        }

        public void SetVisibility(PanelVisibility visibility)
        {
            if (_visibility != visibility)
            {
                _visibility = visibility;
                OnVisibilityChanged();
            }
        }

        private void OnVisibilityChanged()
        {
            gameObject.SetActive(_visibility == PanelVisibility.Visible);
        }
    }
}