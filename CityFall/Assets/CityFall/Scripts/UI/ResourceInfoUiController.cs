﻿using TMPro;
using UnityEngine;

namespace CityFall
{
    public class ResourceInfoUiController : MonoBehaviour, ITick
    {
        [SerializeField] private TMP_Text _energyText;

        [SerializeField] private TMP_Text _minersText;

        [SerializeField] private TMP_Text _casualtiesText;

        public void Tick()
        {
            var city = Singleton<City>.Get();
            _energyText.text = $"{city.Energy.Value:0} / {city.Energy.Capacity}";
            _minersText.text = city.Miners.AwakeMiners.Count.ToString();
            _casualtiesText.text = city.Miners.DeadMinerCount.ToString();
        }
    }
}