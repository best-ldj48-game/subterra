﻿using UnityEngine;
using UnityEngine.UI;

namespace CityFall
{
    public class ToolbarUiController : MonoBehaviour, ITick
    {
        [SerializeField] private Button _engineOnButton;

        [SerializeField] private Button _engineOffButton;

        private void Awake()
        {
            _engineOnButton.onClick.AddListener(OnEngineOnButtonClick);
            _engineOffButton.onClick.AddListener(OnEngineOffButtonClick);
        }

        private void OnEngineOnButtonClick()
        {
            Singleton<City>.Get().Engine.TurnOn();
            AudioManager.instance.uiClick.PlayClip();
        }

        private void OnEngineOffButtonClick()
        {
            Singleton<City>.Get().Engine.TurnOff();
            AudioManager.instance.uiClick.PlayClip();
        }

        public void Tick()
        {
            var city = Singleton<City>.Get();
            _engineOnButton.GetComponent<SelectableUiController>().IsEnabled = !city.Engine.IsOn;
            _engineOffButton.GetComponent<SelectableUiController>().IsEnabled = city.Engine.IsOn;
        }
    }
}