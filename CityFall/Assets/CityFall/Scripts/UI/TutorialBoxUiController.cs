﻿using UnityEngine;
using UnityEngine.UI;

namespace CityFall
{
    public class TutorialBoxUiController : MonoBehaviour, ITick
    {
        private const string Key = "Tutorial";

        [SerializeField] private GameObject _root;

        [SerializeField] private Button _closeButton;

        private bool? _engineOffOldFrame;

        private void Awake()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            _root.gameObject.SetActive(false);
        }

        private void OnCloseButtonClick()
        {
            _root.gameObject.SetActive(false);
        }

        public void Tick()
        {
            if (PlayerPrefs.HasKey(Key)) return;
            _engineOffOldFrame ??= true;
            var engineOn = Singleton<City>.Get().Engine.IsOn;
            if (!_engineOffOldFrame.Value && !engineOn)
            {
                _root.gameObject.SetActive(true);
                PlayerPrefs.SetInt(Key, 1);
                PlayerPrefs.Save();
            }
            _engineOffOldFrame = !engineOn;
        }
    }
}