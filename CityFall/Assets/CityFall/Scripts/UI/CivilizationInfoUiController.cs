﻿using TMPro;
using UnityEngine;

namespace CityFall
{
    public class CivilizationInfoUiController : MonoBehaviour, ITick
    {
        [SerializeField] private TMP_Text _yearText;

        [SerializeField] [TextArea] private string _yearFormat = "Y{0:0000}";

        [SerializeField] private TMP_Text _populationText;

        [SerializeField] [TextArea] private string _populationFormat = "Pop. {0}";

        [SerializeField] private TMP_Text _depthText;

        [SerializeField] [TextArea] private string _depthFormat = "Depth {0}m";

        public void Tick()
        {
            var year = Singleton<TimeState>.Get().IntYear;
            _yearText.text = string.Format(_yearFormat, year);
            var population = Singleton<City>.Get().Population.IntPopulation;
            _populationText.text = string.Format(_populationFormat, population);
            _depthText.text = string.Format(_depthFormat, Singleton<GameState>.Get().DepthInMeters);
        }
    }
}