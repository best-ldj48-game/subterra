﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CityFall
{
    [RequireComponent(typeof(TogglePanel))]
    public class OptionsUiController : MonoBehaviour
    {
        [SerializeField] private Slider _musicSlider;

        [SerializeField] private Slider _sfxSlider;

        [SerializeField] private Button _returnButton;

        private void Awake()
        {
            _musicSlider.value = AudioManager.instance.MusicVolume;
            _sfxSlider.value = AudioManager.instance.SfxVolume;

            _musicSlider.onValueChanged.AddListener(OnMusicValueChanged);
            _sfxSlider.onValueChanged.AddListener(OnSfxValueChanged);
            _returnButton.onClick.AddListener(OnReturnButtonClicked);
        }

        private void OnDisable()
        {
            GetComponent<TogglePanel>().SetVisibility(PanelVisibility.Hidden);
        }

        private void OnReturnButtonClicked()
        {
            GetComponent<TogglePanel>().SetVisibility(PanelVisibility.Hidden);
        }

        private void OnSfxValueChanged(float value)
        {
            AudioManager.instance.SfxVolume = value;
        }

        private void OnMusicValueChanged(float value)
        {
            AudioManager.instance.MusicVolume = value;
        }
    }
}