﻿using System;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CityFall
{
    public class StoryUiController : MonoBehaviour
    {
        [SerializeField] private TMP_Text _storyText;

        [SerializeField] [Tooltip("In seconds")] private float _storyAnimationDurationPerCharacter;

        [SerializeField] private float _timeWaitAfterStoryEnd;

        [SerializeField] [TextArea] private string _story;

        private StringBuilder _storyStringBuilder;

        private float _storyAnimationCounter;

        private float _endWaitCounter;

        private const string TutorialPassedKey = "TutorialPassed";

        private void Awake()
        {
            _storyStringBuilder = new StringBuilder();
            AudioManager.instance.PlayMusic(AudioManager.instance.mainMenuMusicClip);
        }

        private void StartGame()
        {
            if (PlayerPrefs.HasKey(TutorialPassedKey))
            {
                SceneManager.LoadScene(SceneHelper.GameScene);
            }
            else
            {
                PlayerPrefs.SetInt(TutorialPassedKey, 1);
                SceneManager.LoadScene(SceneHelper.TutorialScene);
            }
            
        }

        public void Update()
        {
            _storyAnimationCounter += Time.deltaTime;

            if (_storyAnimationCounter > _storyAnimationDurationPerCharacter)
            {
                if (_storyStringBuilder.Length < _story.Length)
                {
                    _storyStringBuilder.Append(_story[_storyStringBuilder.Length]);
                    _storyAnimationCounter = 0;
                    _storyText.SetText(_storyStringBuilder);
                }
                else
                {
                    _endWaitCounter += Time.deltaTime;
                    if (_endWaitCounter >= _timeWaitAfterStoryEnd)
                    {
                        StartGame();
                    }
                }
            }

            if (Input.GetButtonDown("Skip"))
            {
                StartGame();
            }
        }
    }
}