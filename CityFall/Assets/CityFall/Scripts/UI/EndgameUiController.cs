﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CityFall
{
    public class EndgameUiController : MonoBehaviour
    {
        [SerializeField] private TMP_Text _depthText;

        [SerializeField] private string _depthTextFormat = "Depth {0}m";

        [SerializeField] private Button _tryAgainButton;

        [SerializeField] private Button _mainMenuButton;

        [SerializeField] private Button _tutorialButton;

        [SerializeField] private TMP_Text _quoteSourceText;

        [SerializeField] private TMP_Text _quoteText;

        [SerializeField] private Quote[] _quotes;

        private void Awake()
        {
            _tryAgainButton.onClick.AddListener(OnTryAgainButtonClicked);
            _mainMenuButton.onClick.AddListener(OnMainMenuButtonClicked);
            _tutorialButton.onClick.AddListener(OnTutorialButtonClicked);
        }

        private void OnTutorialButtonClicked()
        {
            SceneManager.LoadScene(SceneHelper.TutorialScene);
        }

        private void OnEnable()
        {
            if (Singleton<GameState>.TryGet(out var gameState)) _depthText.text = string.Format(_depthTextFormat, gameState.DepthInMeters);
            var randomIndex = Random.Range(0, _quotes.Length);
            _quoteText.text = $"“{_quotes[randomIndex].Text}”";
            _quoteSourceText.text = $"- {_quotes[randomIndex].Source}";
        }

        private void OnMainMenuButtonClicked()
        {
            SceneManager.LoadScene(SceneHelper.MainMenuScene);
        }

        private void OnTryAgainButtonClicked()
        {
            SceneManager.LoadScene(SceneHelper.GameScene);
        }

        [Serializable]
        private struct Quote
        {
            [TextArea] public string Text;

            public string Source;
        }
    }
}