﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace CityFall
{
    [RequireComponent(typeof(TogglePanel))]
    public class PauseMenuUiController : MonoBehaviour
    {
        [SerializeField] private Button _resumeButton;

        [SerializeField] private Button _restartButton;

        [SerializeField] private Button _optionsButton;

        [SerializeField] private Button _mainMenuButton;

        [SerializeField] private TogglePanel _optionsPanel;

        private void Awake()
        {
            _resumeButton.onClick.AddListener(OnResumeButtonClicked);
            _restartButton.onClick.AddListener(OnRestartButtonClicked);
            _mainMenuButton.onClick.AddListener(OnMainMenuButtonClicked);
            _optionsButton.onClick.AddListener(OnOptionsButtonClicked);
        }

        private void OnOptionsButtonClicked()
        {
            _optionsPanel.SetVisibility(PanelVisibility.Visible);
        }

        private void OnMainMenuButtonClicked()
        {
            SceneManager.LoadScene(SceneHelper.MainMenuScene);
        }

        private void OnRestartButtonClicked()
        {
            SceneManager.LoadScene(SceneHelper.GameScene);
        }

        private void OnResumeButtonClicked()
        {
            var gameState = Singleton<GameState>.Get();
            gameState.Paused = false;
        }
    }
}