﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CityFall
{
    public class MainMenuUiController : MonoBehaviour
    {
        [SerializeField] private Button _startButton;

        [SerializeField] private Button _exitButton;

        [SerializeField] private Button _optionsButton;

        [SerializeField] private TogglePanel _optionsPanel;

        private void Awake()
        {
            _startButton.onClick.AddListener(OnStartButtonClicked);
            _optionsButton.onClick.AddListener(OnOptionsButtonClicked);
#if !UNITY_EDITOR && UNITY_WEBGL
            _exitButton.gameObject.SetActive(false);
#else
            _exitButton.onClick.AddListener(OnExitButtonClicked);
#endif
            AudioManager.instance.PlayMusic(AudioManager.instance.mainMenuMusicClip);
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel")) _optionsPanel.SetVisibility(PanelVisibility.Hidden);
        }

        private void OnOptionsButtonClicked()
        {
            _optionsPanel.SetVisibility(PanelVisibility.Visible);
        }

        private void OnExitButtonClicked()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private void OnStartButtonClicked()
        {
            SceneManager.LoadScene(SceneHelper.StoryScene);
            AudioManager.instance.startButton.PlayClip();
        }
    }
}